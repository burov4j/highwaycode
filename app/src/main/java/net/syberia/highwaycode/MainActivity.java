package net.syberia.highwaycode;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;
import com.anjlab.android.iab.v3.TransactionDetails;

import net.syberia.highwaycode.billing.GetPaidTicketsTask;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.SubjectType;
import net.syberia.highwaycode.dao.Ticket;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnTicketClickListener, OnSubjectClickListener,
        SubjectsFragment.OnSubjectSelectedListener,
        BillingProcessor.IBillingHandler {

    private static boolean fullVersion = false;

    public static final String TICKETS_PRODUCT_ID = "tickets";

    private final String REFRESHABLE_FRAGMENT_TAG = "refreshable";
    private final String TITLE = "title";

    private ApplicationProperties applicationProperties;
    private BillingProcessor billingProcessor = null;
    private NavigationView navigationView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolBar);
        setSupportActionBar(toolbar);

        applicationProperties = new ApplicationProperties(this);
        billingProcessor = new BillingProcessor(this, hateThieves(), this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
        }

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        if (BillingProcessor.isIabServiceAvailable(this)) {
            boolean purchased = billingProcessor.isPurchased(TICKETS_PRODUCT_ID);
            if (purchased) {
                if (applicationProperties.isFailedPurchasedTicketsInstallation() || !hasPaidTickets()) {
                    installPaidTickets();
                } else {
                    switchToFullVersion();
                }
            }
        }

        if (HighwayCodeDatabaseFactory.isDatabaseExists(this)) {
            if (savedInstanceState == null) {
                showTickets();
                showNewTicketsAvailableDialog();
            }
        } else {
            final MaterialDialog progressDialog = new MaterialDialog.Builder(this)
                    .content("Установка...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColorRes(R.color.material_teal_500)
                    .show();
            new InitDatabaseAsyncTask(this) {
                @Override
                public void onPostExecute(Void aVoid) {
                    progressDialog.hide();
                    showTickets();
                    showNewTicketsAvailableDialog();
                }
            }.execute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putCharSequence(TITLE, getTitle());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setTitle(savedInstanceState.getCharSequence(TITLE));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int menuItemId = item.getItemId();
        showByMenuItemId(menuItemId);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void showByMenuItemId(int id) {
        switch (id) {
            case R.id.highway_code_menu_item:
                showSubjects(SubjectType.RULES);
                break;
            case R.id.signs_menu_item:
                showSubjects(SubjectType.SIGNS);
                break;
            case R.id.marking_menu_item:
                showSubjects(SubjectType.MARKING);
                break;
            case R.id.tickets_menu_item:
                showTickets();
                break;
            case R.id.subjects_menu_item:
                showSubjects();
                break;
            case R.id.mistakes_menu_item:
                showMistakes();
                break;
            case R.id.marathon_menu_item:
                startMarathon();
                break;
            case R.id.statistics_menu_item:
                showStatistics();
                break;
            case R.id.full_version_menu_item:
                purchaseFullVersion();
                break;
            case R.id.rate_menu_item:
                rateApp();
                break;
            // Раскомментировать при появлении настроек
           /* case R.id.settings_menu_item:
                showSettings();
                break; */
            case R.id.about_menu_item:
                showAbout();
                break;
        }
    }

    @Override
    public void OnTicketClick(View caller, Ticket ticket) {
        if (ticket == null) {
            purchaseFullVersion();
        } else {
            Intent intent = new Intent(this, QuestionsActivity.class);
            intent.putExtra(QuestionsActivity.TICKET, ticket);
            startActivity(intent);
        }
    }

    @Override
    public void OnSubjectClick(View caller, Subject subject, boolean training) {
        Intent intent = new Intent(this, QuestionsActivity.class);
        intent.putExtra(QuestionsActivity.SUBJECT, subject);
        startActivity(intent);
    }

    @Override
    public void onSubjectSelected(Subject subject) {
        SubjectsFragment subjectsFragment = (SubjectsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mainActivityContentLayout);
        if (subjectsFragment != null && !subjectsFragment.hasSubjectContentFragment()) {
            Intent intent = new Intent(this, SubjectContentActivity.class);
            intent.putExtra(SubjectContentActivity.SUBJECT, subject);
            startActivity(intent);
        }
    }

    private void showSubjects(byte subjectTypeId) {
        switch (subjectTypeId) {
            case SubjectType.RULES:
                setTitle(getResources().getString(R.string.app_name) + " ― " + "Правила");
                break;
            case SubjectType.SIGNS:
                setTitle(getResources().getString(R.string.app_name) + " ― " + "Знаки");
                break;
            case SubjectType.MARKING:
                setTitle(getResources().getString(R.string.app_name) + " ― " + "Разметка");
                break;
        }
        SubjectsFragment subjectsFragment = new SubjectsFragment();
        Bundle arguments = new Bundle();
        arguments.putByte(SubjectsFragment.SUBJECT_TYPE_ID, subjectTypeId);
        subjectsFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainActivityContentLayout, subjectsFragment)
                .commit();
    }

    private void showTickets() {
        setTitle(getResources().getString(R.string.app_name) + " ― " + "Билеты");
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainActivityContentLayout, new TicketsFragment(), REFRESHABLE_FRAGMENT_TAG)
                .commit();
    }

    private void showSubjects() {
        setTitle(getResources().getString(R.string.app_name) + " ― " + "Темы");
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainActivityContentLayout, new SubjectsWithProgressFragment(), REFRESHABLE_FRAGMENT_TAG)
                .commit();
    }

    private void showMistakes() {
        if (MistakesCountTextView.isEmpty()) {
            AlertDialog.Builder emptyMistakesDialog = new AlertDialog.Builder(this);
            emptyMistakesDialog.setTitle("Работа над ошибками");
            emptyMistakesDialog.setMessage("У вас пока нет ошибок");
            emptyMistakesDialog.setIcon(R.drawable.ic_done_black_48dp);
            emptyMistakesDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            emptyMistakesDialog.show();
        } else {
            Intent intent = new Intent(this, QuestionsActivity.class);
            intent.putExtra(QuestionsActivity.MISTAKES_MODE, true);
            startActivity(intent);
        }
    }

    private void refreshCurrentFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(REFRESHABLE_FRAGMENT_TAG);
        if (fragment != null) {
            fragmentManager.beginTransaction()
                    .detach(fragment)
                    .attach(fragment)
                    .commit();
        }
    }

    private void showAbout() {
        AlertDialog.Builder aboutDialog = new AlertDialog.Builder(this);
        aboutDialog.setTitle("О программе");
        aboutDialog.setView(R.layout.about);
        aboutDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        aboutDialog.show();
    }

    private void rateApp() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + getPackageManager().getPackageInfo(getPackageName(), 0).packageName));
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException ex) {
            throw new RuntimeException("Unable to rate app", ex);
        }
    }

    /**
     * Метод понадобится после появления настроек приложения
     */
    @SuppressWarnings("unused")
    private void showSettings() {
        Intent settingsActivity = new Intent(this, SettingsActivity.class);
        startActivity(settingsActivity);
    }

    private void startMarathon() {
        Intent intent = new Intent(this, MarathonActivity.class);
        startActivity(intent);
    }

    private void showStatistics() {
        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);
    }

    private void purchaseFullVersion() {
        if (BillingProcessor.isIabServiceAvailable(this)) {
            billingProcessor.purchase(this, TICKETS_PRODUCT_ID);
        } else {
            AlertDialog.Builder iabServiceUnavailableDialog = new AlertDialog.Builder(this);
            iabServiceUnavailableDialog.setTitle("Ошибка");
            iabServiceUnavailableDialog.setMessage("Платёжный сервис недоступен");
            iabServiceUnavailableDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            iabServiceUnavailableDialog.show();
        }
    }

    private void switchToFullVersion() {
        fullVersion = true;
        renderVersionSpecificOptions();
    }

    private void renderVersionSpecificOptions() {
        if (navigationView != null) {
            TextView appVersionTextView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.navAppVersionTextView);
            if (fullVersion) {
                appVersionTextView.setText("Полная версия");
                navigationView.getMenu().findItem(R.id.full_version_menu_item).setVisible(false);
            } else {
                appVersionTextView.setText("Демоверсия");
            }
        }
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails transactionDetails) {
        PurchaseInfo purchaseInfo = transactionDetails.purchaseInfo;
        installPaidTickets(purchaseInfo.responseData, purchaseInfo.signature);
    }

    @Override
    public void onPurchaseHistoryRestored() {
        boolean purchased = billingProcessor.isPurchased(TICKETS_PRODUCT_ID);
        if (purchased) {
            if (hasPaidTickets()) {
                switchToFullVersion();
            } else {
                installPaidTickets();
            }
        } else {
            showWelcome();
        }
    }

    private void installPaidTickets() {
        TransactionDetails transactionDetails = billingProcessor.getPurchaseTransactionDetails(TICKETS_PRODUCT_ID);
        PurchaseInfo purchaseInfo = transactionDetails.purchaseInfo;
        installPaidTickets(purchaseInfo.responseData, purchaseInfo.signature);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private void showWelcome() {
        if (!applicationProperties.isWelcomeDialogShown()) {
            // Здесь должно запускаться приветствие
            // App.setWelcomeDialogShown(true);
        }
    }

    /**
     * Закомментированный код необходимо раскомментировать и актуализировать тогда, когда
     * выйдут новые билеты для экзамена и появится необходимость поддерживать старую и новую версии.
     */
    @SuppressWarnings("EmptyMethod")
    private void showNewTicketsAvailableDialog() {
     /*   if (!App.isNewTicketsDialogShown()) {
            AlertDialog.Builder newTicketsAvailableDialog = new AlertDialog.Builder(this);
            newTicketsAvailableDialog.setTitle("Доступны новые билеты");
            newTicketsAvailableDialog.setMessage("С 1 сентября 2016 года при приёме экзамена будут использоваться новые билеты. Экзамены по старым билетам будут проводиться до 31 августа 2016 года. Хотите заниматься по новым билетам?");
            newTicketsAvailableDialog.setIcon(R.drawable.ic_info_black_48dp);
            newTicketsAvailableDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            newTicketsAvailableDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    App.setUseNewTickets(false);
                    MarathonRecordTextView.refresh();
                    MistakesCountTextView.refresh();
                    refreshCurrentFragment();
                    dialog.cancel();
                }
            });
            newTicketsAvailableDialog.show();
            App.setNewTicketsDialogShown(true);
        }*/
    }

    private void installPaidTickets(String purchaseData, String dataSignature) {
        final MaterialDialog progressDialog = new MaterialDialog.Builder(this)
                .content("Загрузка полной версии программы...")
                .progress(true, 0)
                .cancelable(false)
                .widgetColorRes(R.color.material_teal_500)
                .show();
        new GetPaidTicketsTask(this, purchaseData, dataSignature) {
            @Override
            public void onPostExecute(Boolean success) {
                progressDialog.hide();
                if (success) {
                    switchToFullVersion();
                    refreshCurrentFragment();
                    if (applicationProperties.isFailedPurchasedTicketsInstallation()) {
                        applicationProperties.setFailedPurchasedTicketsInstallation(false);
                    }
                    Toast.makeText(MainActivity.this, "Полная версия программы успешно загружена", Toast.LENGTH_LONG).show();
                } else {
                    applicationProperties.setFailedPurchasedTicketsInstallation(true);
                    Toast.makeText(MainActivity.this, "Не удалось загрузить полную версию программы", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        // no operation
    }

    @Override
    public void onBillingInitialized() {
        // no operation
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if (billingProcessor != null) {
            billingProcessor.release();
        }
        super.onDestroy();
    }

    private boolean hasPaidTickets() {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            return highwayCodeDatabaseHelper.getTicketDao().countOf() > 20;
        } catch (SQLException e) {
            throw new RuntimeException("Unable to check if has paid tickets", e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    public static String hateThieves() {
        int param = 23432454;
        char[] chars = "赋赏赏资赏赬赇赈资赡赭起赮赭赯赁贿赱贶资赇赗赃赀赇赇赉赅赇赗贾赇赋赏赏资赅赡赍赅赇赗赃赇贷赗贷购赜赠赠赅走赿赨赞赀赣购赶赶贷赶赼赿走赊走资赼赶赅贴贲赥赂赲赳赎起赟资赓贶赠赪赲赲赴贲赎赐赌赨赕赂赮赩赜赳赞赕赋贷赜赭赫贰贷赌赤赬赾赠赩起贵赞贩贩赊赋赠赌赓贳赞赿赅赣赌贳赭赔赣赴走赡赌贱贴贱赣赧赊赕赧赣赅赉贱赞贵赃赜赕赖走赅贶赥赅赉赢赑赪赓赔赤赯赨赇赪贷赔赀赍赯赊赭贲赓赬赇贵走赂赢贷赋贴赓赣贳赞赞赍起赃赍贶赂资贶赿赧赤赉赓赑赇贩赼赮资赋贿赿赓赱赀贩贶赍贵赜赅赾赗贴赀赴赎赲赗赶赴购贲赳赜赱赣赬贰赬贷贵赏赓赑贰赧起赉赍贾赈赼赡赴赒赓贾贰赁赏赊赍赎赾赿赊赲走赌赓贷赶贳赴赯赾赧贵赡赡赕贶赂赃贶赕赵赓赌赡贳赖赱赶赐赅赲贳赍赳赅赑赉贰赴赪赼贳贩赖赉赫贾资赩赅赅赧赑赧赊贩赫购赱赮赤赞走赡起起赎赐赵贵赶赿赢赎赀赨贶赍赪贵赮赱购赞贳赤赏赐贿贵赑赂赖赩赎赨贳赡赤赁赴赞赒资贩赎赩赳起赇赧赋赗赏赂赇赗赇资"
                .toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (chars[i] ^ param);
        }
        return String.valueOf(chars);
    }

    public static boolean isFullVersion() {
        return fullVersion;
    }

}
