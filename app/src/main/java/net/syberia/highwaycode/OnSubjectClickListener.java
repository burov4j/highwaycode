package net.syberia.highwaycode;

import android.view.View;

import net.syberia.highwaycode.dao.Subject;

/**
 * @author Andrey Burov
 */
interface OnSubjectClickListener {

    void OnSubjectClick(View caller, Subject subject, boolean training);

}
