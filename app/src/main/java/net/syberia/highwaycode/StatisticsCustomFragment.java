package net.syberia.highwaycode;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import net.syberia.highwaycode.dao.CustomStatistics;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;

import java.sql.SQLException;
import java.util.Arrays;

/**
 * @author Andrey Burov
 */
public class StatisticsCustomFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.statistics_custom_progress_fragment,
                container, false);

        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(getContext());
        try {
            CustomStatistics customStatistics = highwayCodeDatabaseHelper.getStatisticsDao().getCustomStatistics();
            PieChart pieChart = (PieChart) rootView;
            Entry[] entries = new Entry[]{
                    new Entry(customStatistics.getPassed(), 0),
                    new Entry(customStatistics.getFailed(), 1),
                    new Entry(customStatistics.getNoResult(), 2)
            };
            String[] labels = new String[] {
                    "Верные", "Неверные", "Без ответа"
            };
            Context context = getContext();
            int[] colors = new int[] {
                    ContextCompat.getColor(context, R.color.material_green_500),
                    ContextCompat.getColor(context, R.color.material_red_500),
                    ContextCompat.getColor(context, R.color.material_grey_500)
            };
            PieDataSet pieDataSet = new PieDataSet(Arrays.asList(entries), null);
            pieDataSet.setColors(colors);
            PieData pieData = new PieData(labels, pieDataSet);
            pieChart.getLegend().setTextSize(16);
            pieChart.setData(pieData);
            pieChart.setDescription(null);
            pieChart.setRotationEnabled(false);
            pieChart.setDrawSliceText(false);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to get custom statistics", e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }

        return rootView;
    }

}
