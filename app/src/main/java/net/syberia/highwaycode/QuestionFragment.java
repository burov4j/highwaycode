package net.syberia.highwaycode;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.syberia.highwaycode.dao.Answer;
import net.syberia.highwaycode.dao.AnswerDao;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Question;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class QuestionFragment extends Fragment {

    public static final String QUESTION = "question";
    public static final String RANDOM_ANSWERS = "random_answers";

    private final String SAVED_ANSWER_NUMBER_KEY = "answer";
    private final byte NO_SAVED_ANSWER_NUMBER = -1;

    private byte savedAnswerNumber = NO_SAVED_ANSWER_NUMBER;

    private OnAnswerSelectedListener callback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question,
                container, false);

        Bundle arguments = getArguments();
        final Question question = (Question) arguments.getSerializable(QUESTION);
        if (question == null) {
            throw new RuntimeException("You must specify '" + QUESTION + "' attribute");
        }

        rootView.setTag(question); // Необходимо для тестирования

        boolean randomAnswers = arguments.getBoolean(RANDOM_ANSWERS, false);

        AppCompatImageView questionImageView = (AppCompatImageView) rootView.findViewById(R.id.questionImageView);
        try {
            Drawable questionImage = HighwayCodeImagesUtils.getImage(getContext(), question);
            questionImageView.setImageDrawable(questionImage);
        } catch (IOException e) {
            Log.e(QuestionFragment.class.getName(), "Unable to load question image", e);
            throw new RuntimeException(e);
        }

        TextView questionTextView = (TextView) rootView.findViewById(R.id.questionTextView);
        questionTextView.setText(question.getText());

        final LinearLayoutCompat questionLayout = (LinearLayoutCompat) rootView.findViewById(R.id.questionLinearLayout);
        List<Answer> answers = getAnswers(question.getId());
        if (randomAnswers) {
            Collections.shuffle(answers);
        }
        final AppCompatButton[] buttons = new AppCompatButton[answers.size()];
        if (savedInstanceState != null) {
            savedAnswerNumber = savedInstanceState.getByte(SAVED_ANSWER_NUMBER_KEY, NO_SAVED_ANSWER_NUMBER);
        }
        for (int i = 0; i < answers.size(); i++) {
            final Answer answer = answers.get(i);
            final AppCompatButton button = new AppCompatButton(getContext());
            button.setText(answer.getText());
            questionLayout.addView(button);
            if (savedAnswerNumber == NO_SAVED_ANSWER_NUMBER) {
                button.setTag(answer);
                buttons[i] = button;
                button.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View sender) {
                                                  savedAnswerNumber = answer.getNumber();
                                                  for (AppCompatButton button : buttons) {
                                                      button.setClickable(false);
                                                  }
                                                  boolean success = answer.getNumber() == question.getRightAnswerNumber();
                                                  if (success) {
                                                      setButtonBackgroundColor(button, R.color.material_green_500);
                                                  } else {
                                                      setButtonBackgroundColor(button, R.color.material_red_500);
                                                      for (AppCompatButton button : buttons) {
                                                          Answer currentButtonAnswer = (Answer) button.getTag();
                                                          if (currentButtonAnswer.getNumber() == question.getRightAnswerNumber()) {
                                                              setButtonBackgroundColor(button, R.color.material_green_500);
                                                              break;
                                                          }
                                                      }
                                                  }
                                                  callback.onAnswerSelected(question, success);
                                              }
                                          }
                );
            } else {
                button.setClickable(false);
                byte currentAnswerNumber = answer.getNumber();
                if (currentAnswerNumber == question.getRightAnswerNumber()) {
                    setButtonBackgroundColor(button, R.color.material_green_500);
                } else if (currentAnswerNumber == savedAnswerNumber) {
                    setButtonBackgroundColor(button, R.color.material_red_500);
                }
            }
        }

        return rootView;
    }

    private void setButtonBackgroundColor(AppCompatButton button, int colorResId) {
        Drawable buttonBackground = button.getBackground();
        int color = ContextCompat.getColor(getContext(), colorResId);
        buttonBackground.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        button.setBackgroundDrawable(buttonBackground);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putByte(SAVED_ANSWER_NUMBER_KEY, savedAnswerNumber);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            callback = (OnAnswerSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnAnswerSelectedListener");
        }
    }

    private List<Answer> getAnswers(int questionId) {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(getContext());
        try {
            AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
            return answerDao.queryForQuestion(questionId);
        } catch (SQLException e) {
            Log.e(QuestionFragment.class.getName(), "Unable to get answers by the question", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    public interface OnAnswerSelectedListener {

        void onAnswerSelected(Question question, boolean success);

    }

}
