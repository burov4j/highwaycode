package net.syberia.highwaycode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Andrey Burov
 */
@SuppressLint("CommitPrefEdits")
class ApplicationProperties {

    private static final String CUSTOM_SETTINGS_FILE_KEY = "custom_settings";

  //  private static final String USE_NEW_TICKETS = "use_new_tickets";

    private static final String IS_WELCOME_DIALOG_SHOWN = "is_welcome_dialog_shown";
    private static final String IS_FAILED_PURCHASED_TICKETS_INSTALLATION = "is_failed_purchased_tickets_installation";
 //   private static final String USE_NEW_TICKETS_DIALOG_SHOWN = "use_new_tickets_dialog_shown";

//    private static final String MARATHON_RECORD = "marathon_record";
    private static final String MARATHON_RECORD_V2 = "marathon_record_v2";

    private final Properties properties = new Properties();
    private final SharedPreferences sharedPreferences;
    private final Context context;

    ApplicationProperties(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(CUSTOM_SETTINGS_FILE_KEY, Context.MODE_PRIVATE);
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open("application.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            Log.e(ApplicationProperties.class.getName(), e.toString());
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    byte getPassPercentage() {
        return Byte.parseByte(properties.getProperty("pass.percentage"));
    }

    int getPassDurationSeconds() {
        return Integer.parseInt(properties.getProperty("pass.duration.seconds"));
    }

    boolean isFailedPurchasedTicketsInstallation() {
        return sharedPreferences.getBoolean(IS_FAILED_PURCHASED_TICKETS_INSTALLATION, false);
    }

    void setFailedPurchasedTicketsInstallation(boolean failedPurchasedTicketsInstallation) {
        sharedPreferences.edit()
                .putBoolean(IS_FAILED_PURCHASED_TICKETS_INSTALLATION, failedPurchasedTicketsInstallation)
                .commit();
    }

    boolean isWelcomeDialogShown() {
        return sharedPreferences.getBoolean(IS_WELCOME_DIALOG_SHOWN, false);
    }

    public void setWelcomeDialogShown(boolean welcomeDialogShown) {
        sharedPreferences.edit()
                .putBoolean(IS_WELCOME_DIALOG_SHOWN, welcomeDialogShown)
                .commit();
    }

    int getMarathonRecord() {
      //  if (isUseNewTickets()) {
            return sharedPreferences.getInt(MARATHON_RECORD_V2, 0);
      /*  } else {
            return sharedPreferences.getInt(MARATHON_RECORD, 0);
        } */
    }

    void setMarathonRecord(int marathonRecord) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
    //    if (isUseNewTickets()) {
            editor = editor.putInt(MARATHON_RECORD_V2, marathonRecord);
    /*    } else {
            editor = editor.putInt(MARATHON_RECORD, marathonRecord);
        } */
        editor.commit();
    }

  /*  boolean isUseNewTickets() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(USE_NEW_TICKETS, true);
    } */

  /*  public void setUseNewTickets(boolean useNewTickets) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit()
                .putBoolean(USE_NEW_TICKETS, useNewTickets)
                .commit();
    }

    public boolean isNewTicketsDialogShown() {
        return sharedPreferences.getBoolean(USE_NEW_TICKETS_DIALOG_SHOWN, false);
    }

    public void setNewTicketsDialogShown(boolean newTicketsDialogShown) {
        sharedPreferences.edit()
                .putBoolean(USE_NEW_TICKETS_DIALOG_SHOWN, newTicketsDialogShown)
                .commit();
    } */

}
