package net.syberia.highwaycode;

import android.content.Context;
import android.os.AsyncTask;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;

/**
 * @author Andrey Burov
 */
abstract class InitDatabaseAsyncTask extends AsyncTask<Void, Void, Void> {

    private final Context context;

    InitDatabaseAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        HighwayCodeDatabaseFactory.initDatabase(context);
        return null;
    }

    @Override
    public abstract void onPostExecute(Void aVoid);

}
