package net.syberia.highwaycode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.syberia.highwaycode.dao.Subject;

import java.util.List;

/**
 * @author Andrey Burov
 */
class SubjectsAdapter extends BaseAdapter {

    private class ViewHolder {

        private final TextView titleTextView;

        ViewHolder(View rootView) {
            this.titleTextView = (TextView) rootView;
        }

    }

    private final List<Subject> subjects;

    SubjectsAdapter(List<Subject> subjects) {
        this.subjects = subjects;
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public Subject getItem(int index) {
        return subjects.get(index);
    }

    @Override
    public long getItemId(int index) {
        return getItem(index).getId();
    }

    @Override
    public View getView(int index, View rootView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (rootView == null) {
            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_item_view, parent, false);
            viewHolder = new ViewHolder(rootView);
            rootView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rootView.getTag();
        }

        Subject subject = getItem(index);
        viewHolder.titleTextView.setText(String.format("%s. %s", subject.getNumber(), subject.getTitle()));

        return rootView;
    }

}
