package net.syberia.highwaycode;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Ticket;
import net.syberia.highwaycode.dao.TicketDao;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class TicketsFragment extends Fragment {

    private OnTicketClickListener callback;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler_view,
                container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new VariableColumnGridLayoutManager(getContext(), 300));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Context context = getContext();
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(context);
        try {
            TicketDao ticketDao = highwayCodeDatabaseHelper.getTicketDao();
            TicketsAdapter adapter = new TicketsAdapter(context, ticketDao.queryForAllWithProgress()) {
                @Override
                public void OnTicketClick(View caller, Ticket ticket) {
                    callback.OnTicketClick(caller, ticket);
                }
            };
            recyclerView.swapAdapter(adapter, false);
        } catch (SQLException e) {
            Log.e(TicketsFragment.class.getName(), "Unable to get tickets list", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            callback = (OnTicketClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnTicketClickListener");
        }
    }

}
