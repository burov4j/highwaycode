package net.syberia.highwaycode;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

/**
 * @author Andrey Burov
 */
class VariableColumnGridLayoutManager extends GridLayoutManager {

    private final int pxMinItemWidth;

    VariableColumnGridLayoutManager(Context context, int dpMinItemWidth) {
        super(context, 1);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.pxMinItemWidth = (int)(dpMinItemWidth * displayMetrics.density);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        updateSpanCount();
        super.onLayoutChildren(recycler, state);
    }

    private void updateSpanCount() {
        int spanCount = getWidth() / pxMinItemWidth;
        if (spanCount < 1) {
            spanCount = 1;
        }
        this.setSpanCount(spanCount);
    }

}
