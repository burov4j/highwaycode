package net.syberia.highwaycode;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.TicketWithProgress;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class StatisticsTicketsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.statistics_tickets_progress_fragment,
                container, false);

        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(getContext());
        try {
            List<TicketWithProgress> ticketsWithProgress = highwayCodeDatabaseHelper.getTicketDao().queryForAllWithProgress();
            HorizontalBarChart barChart = (HorizontalBarChart) rootView;
            List<BarEntry> entries = new ArrayList<>(ticketsWithProgress.size());
            List<String> xValues = new ArrayList<>(ticketsWithProgress.size());
            for (TicketWithProgress ticketWithProgress : ticketsWithProgress) {
                byte ticketNumber = ticketWithProgress.getNumber();
                float progressPercentage = ticketWithProgress.getCountOfPassedQuestions() * 100.0f / ticketWithProgress.getCountOfQuestions();
                BarEntry barEntry = new BarEntry(progressPercentage, ticketNumber - 1);
                entries.add(barEntry);
                xValues.add(String.valueOf(ticketNumber));
            }

            BarDataSet barDataSet = new BarDataSet(entries, null);
            BarData barData = new BarData(xValues, barDataSet);
            barChart.setData(barData);
            barChart.setDoubleTapToZoomEnabled(false);
            barChart.setDescription(null);
            barChart.setMaxVisibleValueCount(0);
            barChart.getAxisRight().setEnabled(false);
            barChart.getLegend().setEnabled(false);

            YAxis yAxis = barChart.getAxisLeft();
            yAxis.setAxisMinValue(0);
            yAxis.setAxisMaxValue(100);
            yAxis.setValueFormatter(new PercentFormatter(new DecimalFormat("###,###,##0")));

            barChart.setVisibleYRangeMaximum(yAxis.mAxisRange / 2.0f, YAxis.AxisDependency.RIGHT);

            XAxis xAxis = barChart.getXAxis();
            xAxis.setValueFormatter(new NumberValueFormatter());
            xAxis.setXOffset(10);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to get tickets statistics", e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }

        return rootView;
    }

}
