package net.syberia.highwaycode;

import android.view.View;

import net.syberia.highwaycode.dao.Ticket;

/**
 * @author Andrey Burov
 */
interface OnTicketClickListener {

    void OnTicketClick(View caller, Ticket ticket);

}
