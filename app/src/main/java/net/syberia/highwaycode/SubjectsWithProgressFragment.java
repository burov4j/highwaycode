package net.syberia.highwaycode;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.SubjectDao;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class SubjectsWithProgressFragment extends Fragment {

    private OnSubjectClickListener callback;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler_view,
                container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new VariableColumnGridLayoutManager(getContext(), 300));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Context context = getContext();
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(context);
        try {
            SubjectDao subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
            SubjectsWithProgressAdapter adapter = new SubjectsWithProgressAdapter(context, subjectDao.queryForAllWithProgress()) {
                @Override
                public void OnSubjectClick(View caller, Subject subject, boolean training) {
                    callback.OnSubjectClick(caller, subject, training);
                }
            };
            recyclerView.swapAdapter(adapter, false);
        } catch (SQLException e) {
            Log.e(SubjectsWithProgressFragment.class.getName(), "Unable to get subjects list", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            callback = (OnSubjectClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnSubjectClickListener");
        }
    }

}
