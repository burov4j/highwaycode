package net.syberia.highwaycode;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.QuestionDao;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class MistakesCountTextView extends AppCompatTextView {

    private static MistakesCountTextView mistakesCountTextView = null;
    private static long mistakesCount = 0;

    public MistakesCountTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mistakesCountTextView = this;
        refresh(context);
    }

    public static void refresh(Context context) {
        if (mistakesCountTextView == null) {
            return;
        }
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory
                .getHighwayCodeDatabaseHelper(context);
        try {
            QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
            mistakesCount = questionDao.queryForMistakesCount();
            if (isEmpty()) {
                mistakesCountTextView.setVisibility(View.INVISIBLE);
            } else {
                mistakesCountTextView.setText(String.valueOf(mistakesCount));
                mistakesCountTextView.setVisibility(View.VISIBLE);
            }
        } catch (SQLException e) {
            Log.e(QuestionsActivity.class.getName(), "Unable to get mistaken questions count", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    public static boolean isEmpty() {
        return mistakesCount < 1;
    }

}
