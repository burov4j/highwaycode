package net.syberia.highwaycode;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.SubjectDao;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class SubjectsFragment extends Fragment {

    public static final String SUBJECT_TYPE_ID = "subject_type_id";
    private static final String SELECTED_SUBJECT = "selected_subject";

    private OnSubjectSelectedListener callback;

    private SubjectContentFragment subjectContentFragment = null;

    private Subject selectedSubject = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.subjects_fragment, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.subjectsListView);

        subjectContentFragment = (SubjectContentFragment) getChildFragmentManager().findFragmentById(R.id.subjectsContentFragment);

        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(getContext());
        try {
            SubjectDao subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
            List<Subject> subjects = subjectDao.queryForSubjectTypeId(getArguments().getByte(SUBJECT_TYPE_ID));
            SubjectsAdapter adapter = new SubjectsAdapter(subjects);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
                    Subject subject = (Subject) adapterView.getItemAtPosition(index);
                    selectedSubject = subject;
                    callback.onSubjectSelected(subject);
                    if (hasSubjectContentFragment()) {
                        subjectContentFragment.loadSubjectContent(subject);
                    }
                }
            });

            if (savedInstanceState != null) {
                selectedSubject = (Subject) savedInstanceState.getSerializable(SELECTED_SUBJECT);
            }

            if (hasSubjectContentFragment()) {
                if (selectedSubject == null) {
                    if (!subjects.isEmpty()) {
                        subjectContentFragment.loadSubjectContent(subjects.get(0));
                    }
                } else {
                    subjectContentFragment.loadSubjectContent(selectedSubject);
                }
            }
        } catch (SQLException e) {
            Log.e(SubjectsFragment.class.getName(), "Unable to get subjects list", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }

        return rootView;
    }

    public boolean hasSubjectContentFragment() {
        return subjectContentFragment != null && subjectContentFragment.isInLayout();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (OnSubjectSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnSubjectClickListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SELECTED_SUBJECT, selectedSubject);
        super.onSaveInstanceState(outState);
    }

    public interface OnSubjectSelectedListener {

        void onSubjectSelected(Subject subject);

    }

}
