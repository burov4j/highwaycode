package net.syberia.highwaycode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import net.syberia.highwaycode.dao.Subject;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author Andrey Burov
 */
public class SubjectContentFragment extends Fragment {

    public static final String SUBJECT = "subject";

    private WebView webView;

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_subject_content, container, false);
        webView = (WebView) rootView.findViewById(R.id.subjectContentWebView);
        ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.subjectContentProgressBar);
        int progressBarColor = ContextCompat.getColor(getContext(), R.color.material_indigo_500);
        progressBar.getProgressDrawable().setColorFilter(progressBarColor, PorterDuff.Mode.SRC_IN);
        webView.setWebChromeClient(new ProgressWebClient(progressBar));
        webView.addJavascriptInterface(this, "context");
        WebSettings webSettings = webView.getSettings();
        //noinspection deprecation
        webSettings.setTextSize(WebSettings.TextSize.NORMAL); // Метод setTextZoom требует минимум API 14
        webSettings.setJavaScriptEnabled(true);
        Bundle arguments = getArguments();
        if (arguments == null) {
            return rootView;
        }

        Subject subject = (Subject) arguments.getSerializable(SUBJECT);
        if (subject == null) {
            return rootView;
        }

        loadSubjectContent(subject);

        return rootView;
    }

    public void loadSubjectContent(Subject subject) {
        webView.loadDataWithBaseURL("file:///android_asset/", getSubjectContent(subject.getContentPath()),
                "text/html", "UTF-8", null);
    }

    private String getSubjectContent(String subjectContentPath) {
        InputStream inputStream = null;
        try {
            Context context = getContext();
            if (context == null) {
                return null;
            } else {
                inputStream = context.getAssets().open(subjectContentPath);
                return IOUtils.toString(inputStream, Charset.defaultCharset());
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to get subject content", e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    @JavascriptInterface
    public void showImage(String imagePath) throws IOException {
        Context context = getContext();
        AppCompatImageView imageView = new AppCompatImageView(context);
        Drawable drawable = HighwayCodeImagesUtils.getImage(context, imagePath);
        imageView.setImageDrawable(drawable);
        new AlertDialog.Builder(getContext())
                .setView(imageView)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

}
