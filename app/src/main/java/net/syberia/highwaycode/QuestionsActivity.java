package net.syberia.highwaycode;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Question;
import net.syberia.highwaycode.dao.QuestionDao;
import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.Ticket;
import net.syberia.highwaycode.dao.TicketDao;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Burov
 */
public class QuestionsActivity extends AppCompatActivity
        implements QuestionFragment.OnAnswerSelectedListener {

    public static final String TICKET = "ticket";
    public static final String SUBJECT = "subject";
    public static final String MISTAKES_MODE = "mistakes_mode";

    private static final String PROGRESS = "progress";
    private static final String START_DATE = "start_date";

    private ApplicationProperties applicationProperties;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private List<Question> questions;
    private Mode mode = Mode.TICKET;

    @SuppressLint("UseSparseArrays") // SparseArray нельзя поместить в Bundle напрямую
    private HashMap<Integer, Boolean> progress = new HashMap<>();
    private Date startDate = new Date();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applicationProperties = new ApplicationProperties(this);
        setContentView(R.layout.activity_ticket_questions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.questionsToolBar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int statusBarColor = ContextCompat.getColor(this, R.color.material_teal_900);
            getWindow().setStatusBarColor(statusBarColor); // Иначе StatusBar будет прозрачным
        }

        Intent intent = getIntent();
        Ticket ticket = (Ticket) intent.getSerializableExtra(TICKET);
        if (ticket == null) {
            Subject subject = (Subject) intent.getSerializableExtra(SUBJECT);
            if (subject == null) {
                boolean mistakesMode = intent.getBooleanExtra(MISTAKES_MODE, false);
                if (mistakesMode) {
                    mode = Mode.MISTAKES;
                    setTitle(getResources().getString(R.string.app_name) + " ― Работа над ошибками");
                    questions = getMistakenQuestions();
                } else {
                    throw new RuntimeException("You must specify '" + TICKET + "' or '" + SUBJECT + "' attributes");
                }
            } else {
                mode = Mode.SUBJECT;
                setTitle(getResources().getString(R.string.app_name) + " ― " + subject.getTitle());
                questions = getQuestionsBySubject(subject.getId());
            }
        } else {
            byte ticketNumber = ticket.getNumber();
            setTitle(getResources().getString(R.string.app_name) + " ― Билет №" + ticketNumber);
            questions = getQuestionsByTicket(ticketNumber);
        }

        tabLayout = (TabLayout) findViewById(R.id.ticketQuestionsTabLayout);
        for (int questionNumber = 1; questionNumber <= questions.size(); questionNumber++) {
            tabLayout.addTab(tabLayout.newTab().setText(String.valueOf(questionNumber)));
        }

        viewPager = (ViewPager) findViewById(R.id.ticketQuestionsViewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setAdapter(new TicketQuestionsPagerAdapter(getSupportFragmentManager(), questions));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // no operation
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // no operation
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.questions_menu, menu);
        return true;
    }

    private List<Question> getQuestionsByTicket(byte ticketNumber) {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
            return questionDao.queryForTicket(ticketNumber);
        } catch (SQLException e) {
            Log.e(QuestionsActivity.class.getName(), "Unable to get questions by ticket", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    private List<Question> getQuestionsBySubject(byte subjectId) {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
            return questionDao.queryForSubject(subjectId);
        } catch (SQLException e) {
            Log.e(QuestionsActivity.class.getName(), "Unable to get questions by subject", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    private List<Question> getMistakenQuestions() {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
            return questionDao.queryForMistaken();
        } catch (SQLException e) {
            Log.e(QuestionsActivity.class.getName(), "Unable to get mistaken questions", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(PROGRESS, progress);
        outState.putSerializable(START_DATE, startDate);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //noinspection unchecked
        progress = (HashMap<Integer, Boolean>) savedInstanceState.getSerializable(PROGRESS);
        startDate = (Date) savedInstanceState.getSerializable(START_DATE);
        for (Integer questionId : progress.keySet()) {
            int questionIndex = getQuestionIndexById(questionId);
            TabLayout.Tab tab = tabLayout.getTabAt(questionIndex);
            if (tab != null) {
                if (progress.get(questionId)) {
                    tab.setIcon(R.drawable.ic_done_white_48dp);
                } else {
                    tab.setIcon(R.drawable.ic_clear_white_48dp);
                }
            }
        }
    }

    private int getQuestionIndexById(int questionId) {
        for (int questionIndex = 0; questionIndex < questions.size(); questionIndex++) {
            Question question = questions.get(questionIndex);
            if (question.getId() == questionId) {
                return questionIndex;
            }
        }
        return -1;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_hint:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Справка");
                alert.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                WebView webView = new WebView(this);
                webView.loadDataWithBaseURL("file:///android_asset/", questions.get(tabLayout.getSelectedTabPosition()).getComment(),
                        "text/html", "UTF-8", null);
                alert.setView(webView);
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAnswerSelected(Question question, boolean success) {
        int questionId = question.getId();
        if (!isAnswered(questionId)) {
            Date answerDate = new Date();
            progress.put(questionId, success);
            int questionIndex = getQuestionIndexById(questionId);
            TabLayout.Tab tab = tabLayout.getTabAt(questionIndex);
            if (tab != null) {
                if (success) {
                    tab.setIcon(R.drawable.ic_done_white_48dp);
                } else {
                    tab.setIcon(R.drawable.ic_clear_white_48dp);
                }
            }
            HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
            try {
                QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
                question.setSuccess(success);
                questionDao.update(question);
                MistakesCountTextView.refresh(this);
            } catch (SQLException e) {
                Log.e(QuestionFragment.class.getName(), "Unable to update question");
                throw new RuntimeException(e);
            } finally {
                highwayCodeDatabaseHelper.close();
            }

            if (isCompleted()) {
                switch (mode) {
                    case TICKET:
                        completeTicket(answerDate, question.getTicket().getNumber());
                        break;
                    case SUBJECT:
                        completeSubject(answerDate);
                        break;
                    case MISTAKES:
                        completeFixingMistakes(answerDate);
                        break;
                }
                return;
            }

            nextQuestion();
        }
    }

    private void completeTicket(Date answerDate, byte ticketNumber) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        long durationSeconds = (answerDate.getTime() - startDate.getTime()) / 1000;
        if (isPassed()) {
            boolean durationPassed = isDurationPassed(durationSeconds);
            updateDurationPassed(ticketNumber, durationPassed);
            if (durationPassed) {
                alert.setTitle("Билет пройден");
                alert.setIcon(R.drawable.ic_done_black_48dp);
            } else {
                alert.setTitle("Билет не пройден");
                alert.setIcon(R.drawable.ic_clear_black_48dp);
            }
        } else {
            alert.setTitle("Билет не пройден");
            alert.setIcon(R.drawable.ic_clear_black_48dp);
        }
        int passDurationSeconds = applicationProperties.getPassDurationSeconds();
        String dialogMessage = String.format(Locale.getDefault(),
                "Правильных ответов: %s из %s\nПотрачено времени: %02d:%02d\nЛимит времени: %02d:%02d",
                getPassedCount(), questions.size(), durationSeconds / 60, durationSeconds % 60,
                passDurationSeconds / 60, passDurationSeconds % 60);
        alert.setMessage(dialogMessage);
        alert.setPositiveButton("Завершить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        alert.show();
    }

    private void completeSubject(Date answerDate) {
        showCompleteMessage(answerDate, "Тема пройдена", "Тема не пройдена");
    }

    private void completeFixingMistakes(Date answerDate) {
        showCompleteMessage(answerDate, "Работа над ошибками закончена", "Работа над ошибками не закончена");
    }

    private void showCompleteMessage(Date answerDate, String successMessage, String failMessage) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if (isPassed()) {
            alert.setTitle(successMessage);
            alert.setIcon(R.drawable.ic_done_black_48dp);
        } else {
            alert.setTitle(failMessage);
            alert.setIcon(R.drawable.ic_clear_black_48dp);
        }
        long durationSeconds = (answerDate.getTime() - startDate.getTime()) / 1000;
        String dialogMessage = String.format(Locale.getDefault(),
                "Правильных ответов: %s из %s\nПотрачено времени: %02d:%02d",
                getPassedCount(), questions.size(), durationSeconds / 60, durationSeconds % 60);
        alert.setMessage(dialogMessage);
        alert.setPositiveButton("Завершить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
        alert.show();
    }

    private void nextQuestion() {
        int currentQuestionIndex = viewPager.getCurrentItem(),
                nextQuestionIndex = currentQuestionIndex;
        do {
            if (nextQuestionIndex == questions.size() - 1) {
                nextQuestionIndex = 0;
            } else {
                nextQuestionIndex++;
            }
            if (!isAnswered(questions.get(nextQuestionIndex).getId())) {
                viewPager.setCurrentItem(nextQuestionIndex);
                return;
            }
        } while (nextQuestionIndex != currentQuestionIndex);
    }

    private boolean isAnswered(int questionId) {
        return progress.get(questionId) != null;
    }

    private boolean isCompleted() {
        return progress.size() == questions.size();
    }

    private byte getPassedCount() {
        byte passed = 0;
        for (Integer questionId : progress.keySet()) {
            if (progress.get(questionId)) {
                passed++;
            }
        }
        return passed;
    }

    private boolean isPassed() {
        return getPassedCount() * 100 / progress.size() >= applicationProperties.getPassPercentage();
    }

    private boolean isDurationPassed(long durationSeconds) {
        return durationSeconds <= applicationProperties.getPassDurationSeconds();
    }

    private void updateDurationPassed(byte ticketNumber, boolean durationPassed) {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            TicketDao ticketDao = highwayCodeDatabaseHelper.getTicketDao();
            ticketDao.updateDurationPassed(ticketNumber, durationPassed);
        } catch (SQLException e) {
            Log.e(QuestionFragment.class.getName(), "Unable to update duration passed of the ticket", e);
            throw new RuntimeException(e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    private enum Mode {
        TICKET, SUBJECT, MISTAKES
    }

}
