package net.syberia.highwaycode;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Andrey Burov
 */
public class MarathonRecordTextView extends AppCompatTextView {

    private static MarathonRecordTextView marathonRecordTextView = null;

    public MarathonRecordTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        marathonRecordTextView = this;
        refresh(context);
    }

    public static void refresh(Context context) {
        ApplicationProperties applicationProperties = new ApplicationProperties(context);
        int marathonRecord = applicationProperties.getMarathonRecord();
        if (marathonRecord > 0) {
            marathonRecordTextView.setVisibility(View.VISIBLE);
            marathonRecordTextView.setText(String.valueOf(marathonRecord));
        } else {
            marathonRecordTextView.setVisibility(View.INVISIBLE);
        }
    }

}
