package net.syberia.highwaycode;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import net.syberia.highwaycode.dao.Subject;

/**
 * @author Andrey Burov
 */
public class SubjectContentActivity extends AppCompatActivity {

    public static final String SUBJECT = "subject";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.subjectContentToolBar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Subject subject = (Subject) getIntent().getSerializableExtra(SUBJECT);
        setTitle(getString(R.string.app_name) + " ― " + subject.getTitle());
        SubjectContentFragment subjectContentFragment = new SubjectContentFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(SubjectContentFragment.SUBJECT, subject);
        subjectContentFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.subjectContentDrawerLayout, subjectContentFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
