package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class SubjectDao extends BaseDaoImpl<Subject, Byte> {

    SubjectDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Subject.class);
    }

    public List<SubjectWithProgress> queryForAllWithProgress() throws SQLException {
        return new SubjectWithProgressDao(connectionSource).queryForAll();
    }

    public List<Subject> queryForSubjectTypeId(byte subjectTypeId) throws SQLException {
        return queryForEq(Subject.FIELD_NAME_SUBJECT_TYPE_ID, subjectTypeId);
    }

    private class SubjectWithProgressDao extends BaseDaoImpl<SubjectWithProgress, Byte> {

        SubjectWithProgressDao(ConnectionSource connectionSource) throws SQLException {
            super(connectionSource, SubjectWithProgress.class);
        }

    }

}
