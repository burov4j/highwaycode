package net.syberia.highwaycode.dao;

import android.content.Context;

import java.io.File;

/**
 * Класс создан для инкапсулирования логики переключения между старой и новой версиями билетов.
 * При поддержке одновременно двух версий необходимо раскомментировать код,
 * а также внести в него следующие изменения:
 * 1) К переменным, отвечающим за старую версию (закомментированы), необходимо добавить суффикс V2;
 * 2) У переменных, отвечающих за новую версию, необходимо изменить суффикс на V3;
 * 3) Необходимо также изменить имена файлов с *.sql-скриптами и базами данных с
 * соответствующими суффиксами
 *
 * @author Andrey Burov
 */
public class HighwayCodeDatabaseFactory {
  /*
    private static final String DATABASE_NAME = "highway_code.db";
    private static final int DATABASE_VERSION = 1;
    private static final String INIT_SQL_FILE_NAME = "highway_code_database.sql";
    */

    private static final String DATABASE_NAME_V2 = "highway_code_v2.db";
    private static final int DATABASE_VERSION_V2 = 1;
    private static final String INIT_SQL_FILE_NAME_V2 = "highway_code_database_v2.sql";

    private HighwayCodeDatabaseFactory() {
        super();
    }

    public static HighwayCodeDatabaseHelper getHighwayCodeDatabaseHelper(Context context) {
     //   if (App.isUseNewTickets()) {
            return getNewHighwayCodeDatabaseHelper(context);
     //   } else {
     //       return getOldHighwayCodeDatabaseHelper(context);
     //   }
    }

    public static HighwayCodeDatabaseHelper getNewHighwayCodeDatabaseHelper(Context context) {
        return new HighwayCodeDatabaseHelper(context, DATABASE_NAME_V2, DATABASE_VERSION_V2, INIT_SQL_FILE_NAME_V2);
    }

  /*  public static HighwayCodeDatabaseHelper getOldHighwayCodeDatabaseHelper(Context context) {
        return new HighwayCodeDatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION, INIT_SQL_FILE_NAME);
    } */

    public static boolean isDatabaseExists(Context context) {
        File //database = context.getDatabasePath(DATABASE_NAME),
                databaseV2 = context.getDatabasePath(DATABASE_NAME_V2);
        return //database.exists() &&
                databaseV2.exists();
    }

    public static void initDatabase(Context context) {
        HighwayCodeDatabaseHelper //highwayCodeDatabaseHelper = new HighwayCodeDatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION, INIT_SQL_FILE_NAME),
                highwayCodeDatabaseHelperV2 = new HighwayCodeDatabaseHelper(context, DATABASE_NAME_V2, DATABASE_VERSION_V2, INIT_SQL_FILE_NAME_V2);
        try {
         //   highwayCodeDatabaseHelper.initDatabase();
            highwayCodeDatabaseHelperV2.initDatabase();
        } finally {
         //   highwayCodeDatabaseHelper.close();
            highwayCodeDatabaseHelperV2.close();
        }
    }

}
