package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "tickets_with_progress")
public class TicketWithProgress extends Ticket {

    @DatabaseField(columnName = "questions_count")
    private byte countOfQuestions;

    @DatabaseField(columnName = "passed_questions_count")
    private byte countOfPassedQuestions;

    public byte getCountOfPassedQuestions() {
        return countOfPassedQuestions;
    }

    public void setCountOfPassedQuestions(byte countOfPassedQuestions) {
        this.countOfPassedQuestions = countOfPassedQuestions;
    }

    public byte getCountOfQuestions() {
        return countOfQuestions;
    }

    public void setCountOfQuestions(byte countOfQuestions) {
        this.countOfQuestions = countOfQuestions;
    }

}
