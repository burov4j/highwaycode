package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "tickets")
public class Ticket implements Serializable {

    static final String FIELD_NAME_DURATION_PASSED = "duration_passed";
    static final String FIELD_NAME_NUMBER = "number";

    @DatabaseField(id = true)
    private byte number;

    @DatabaseField(columnName = FIELD_NAME_DURATION_PASSED)
    private Boolean durationPassed;

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public Boolean getDurationPassed() {
        return durationPassed;
    }

    public void setDurationPassed(Boolean durationPassed) {
        this.durationPassed = durationPassed;
    }

}
