package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "answers")
public class Answer {

    static final String FIELD_NAME_QUESTION_ID = "question_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private byte number;

    @DatabaseField(canBeNull = false)
    private String text;

    @DatabaseField(foreign = true, foreignColumnName = "id", canBeNull = false)
    private Question question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
