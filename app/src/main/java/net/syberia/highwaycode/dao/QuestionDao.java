package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class QuestionDao extends BaseDaoImpl<Question, Integer> {

    public QuestionDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Question.class);
    }

    public List<Question> queryForMistaken() throws SQLException {
        return this.queryForEq(Question.FIELD_NAME_SUCCESS, false);
    }

    public long queryForMistakesCount() throws SQLException {
        return this.queryBuilder().where().eq(Question.FIELD_NAME_SUCCESS, false).countOf();
    }

    public List<Question> queryForTicket(byte ticketNumber) throws SQLException {
        return this.queryForEq(Question.FIELD_NAME_TICKET_NUMBER, ticketNumber);
    }

    public List<Question> queryForSubject(byte subjectId) throws SQLException {
        QueryBuilder<QuestionsSubjectsLinks, Integer> linksQueryBuilder = new QuestionsSubjectsLinksDao(connectionSource).queryBuilder();
        linksQueryBuilder.where().eq(QuestionsSubjectsLinks.FIELD_NAME_SUBJECT_ID, subjectId);
        QueryBuilder<Question, Integer> subjectsQueryBuilder = this.queryBuilder();
        subjectsQueryBuilder.join(linksQueryBuilder);
        return subjectsQueryBuilder.query();
    }

    public Question queryForRandomQuestion(List<Integer> excludedQuestionsIds) throws SQLException {
        String queryForAll = "SELECT " + Question.FIELD_NAME_QUESTION_ID +
                " FROM " + Question.TABLE_NAME + " ORDER BY RANDOM()";
        GenericRawResults<Integer> questionsIds = this.queryRaw(queryForAll, new RawRowMapper<Integer>() {
            @Override
            public Integer mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
                return Integer.parseInt(resultColumns[0]);
            }
        });
        for (Integer questionId : questionsIds) {
            if (!excludedQuestionsIds.contains(questionId)) {
                return queryForId(questionId);
            }
        }
        throw new SQLException("No questions found by the excluded identifiers");
    }

    public Question queryForRandomQuestion() throws SQLException {
        return this.queryBuilder().orderByRaw("RANDOM()").queryForFirst();
    }

    public void clearProgress() throws SQLException {
        UpdateBuilder<Question, Integer> updateBuilder = this.updateBuilder();
        updateBuilder.updateColumnValue(Question.FIELD_NAME_SUCCESS, null);
        updateBuilder.update();
    }

}
