package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "custom_statistics")
public class CustomStatistics implements Serializable {

    @DatabaseField
    private int passed;

    @DatabaseField
    private int failed;

    @DatabaseField(columnName = "no_result")
    private int noResult;

    public int getFailed() {
        return failed;
    }

    public void setFailed(int failed) {
        this.failed = failed;
    }

    public int getNoResult() {
        return noResult;
    }

    public void setNoResult(int noResult) {
        this.noResult = noResult;
    }

    public int getPassed() {
        return passed;
    }

    public void setPassed(int passed) {
        this.passed = passed;
    }

}
