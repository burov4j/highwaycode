package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "questions_subjects_links")
class QuestionsSubjectsLinks implements Serializable {

    static final String FIELD_NAME_QUESTION_ID = "question_id";
    static final String FIELD_NAME_SUBJECT_ID = "subject_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignColumnName = "id", canBeNull = false)
    private Question question;

    @DatabaseField(foreign = true, foreignColumnName = "id", canBeNull = false)
    private Subject subject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

}
