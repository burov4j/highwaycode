package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class TicketDao extends BaseDaoImpl<Ticket, Byte> {

    TicketDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Ticket.class);
    }

    public void updateDurationPassed(byte ticketNumber, boolean durationPassed) throws SQLException {
        UpdateBuilder<Ticket, Byte> updateBuilder = updateBuilder();
        updateBuilder.updateColumnValue(Ticket.FIELD_NAME_DURATION_PASSED, durationPassed);
        updateBuilder.where().eq(Ticket.FIELD_NAME_NUMBER, ticketNumber);
        updateBuilder.update();
    }

    public List<TicketWithProgress> queryForAllWithProgress() throws SQLException {
        return new TicketWithProgressDao(connectionSource).queryForAll();
    }

    public void deletePaidTickets() throws SQLException {
        DeleteBuilder<Ticket, Byte> deleteBuilder = this.deleteBuilder();
        deleteBuilder.where().gt(Ticket.FIELD_NAME_NUMBER, 20);
        deleteBuilder.delete();
    }

    void clearProgress() throws SQLException {
        UpdateBuilder<Ticket, Byte> updateBuilder = this.updateBuilder();
        updateBuilder.updateColumnValue(Ticket.FIELD_NAME_DURATION_PASSED, null);
        updateBuilder.update();
    }

    private class TicketWithProgressDao extends BaseDaoImpl<TicketWithProgress, Byte> {

        TicketWithProgressDao(ConnectionSource connectionSource) throws SQLException {
            super(connectionSource, TicketWithProgress.class);
        }

    }

}
