package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = Question.TABLE_NAME)
public class Question implements Serializable {

    static final String TABLE_NAME = "questions";
    static final String FIELD_NAME_QUESTION_ID = "id";
    static final String FIELD_NAME_TICKET_NUMBER = "ticket_number";
    static final String FIELD_NAME_QUESTION_TEXT = "text";
    static final String FIELD_NAME_SUCCESS = "success";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private byte number;

    @DatabaseField(canBeNull = false)
    private String text;

    @DatabaseField(columnName = "right_answer_number")
    private byte rightAnswerNumber;

    @DatabaseField
    private Boolean success;

    @DatabaseField(canBeNull = false)
    private String comment;

    @DatabaseField(foreign = true, foreignColumnName = "number", canBeNull = false)
    private Ticket ticket;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public byte getRightAnswerNumber() {
        return rightAnswerNumber;
    }

    public void setRightAnswerNumber(byte rightAnswerNumber) {
        this.rightAnswerNumber = rightAnswerNumber;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
