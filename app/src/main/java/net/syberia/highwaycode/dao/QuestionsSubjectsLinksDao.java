package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
class QuestionsSubjectsLinksDao extends BaseDaoImpl<QuestionsSubjectsLinks, Integer> {

    QuestionsSubjectsLinksDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, QuestionsSubjectsLinks.class);
    }

}
