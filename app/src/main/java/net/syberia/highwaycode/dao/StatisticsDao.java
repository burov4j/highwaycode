package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class StatisticsDao {

    private final ConnectionSource connectionSource;

    StatisticsDao(ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }

    public CustomStatistics getCustomStatistics() throws SQLException {
        CustomStatisticsDao customStatisticsDao = new CustomStatisticsDao(connectionSource);
        return customStatisticsDao.queryForFirst(customStatisticsDao.queryBuilder().prepare());
    }

    ConnectionSource getConnectionSource() {
        return connectionSource;
    }

    private class CustomStatisticsDao extends BaseDaoImpl<CustomStatistics, Integer> {

        CustomStatisticsDao(ConnectionSource connectionSource) throws SQLException {
            super(connectionSource, CustomStatistics.class);
        }

    }

}
