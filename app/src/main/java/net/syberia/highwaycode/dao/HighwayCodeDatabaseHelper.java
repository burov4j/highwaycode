package net.syberia.highwaycode.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public class HighwayCodeDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private final Context context;
    private final String initSqlFileName;

    HighwayCodeDatabaseHelper(Context context, String databaseName, int databaseVersion, String initSqlFileName) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
        this.initSqlFileName = initSqlFileName;
    }

    void initDatabase() {
        getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(initSqlFileName);
            String multipleSql = IOUtils.toString(inputStream, Charset.forName("UTF-8"));
            executeMultipleSql(database, multipleSql);
        } catch (IOException e) {
            Log.e(HighwayCodeDatabaseHelper.class.getName(), "Unable to read file '" + initSqlFileName + "'", e);
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public void executeMultipleSql(String multipleSql) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        executeMultipleSql(sqLiteDatabase, multipleSql);
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    private void executeMultipleSql(SQLiteDatabase database, String multipleSql) {
        String[] sqlStatements = multipleSql.split(";\\r?\\n");
        for (String singleSqlStatement : sqlStatements) {
            database.execSQL(singleSqlStatement);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        // no operation
    }

    public TicketDao getTicketDao() throws SQLException {
        return new TicketDao(connectionSource);
    }

    public QuestionDao getQuestionDao() throws SQLException {
        return new QuestionDao(connectionSource);
    }

    public AnswerDao getAnswerDao() throws SQLException {
        return new AnswerDao(connectionSource);
    }

    public SubjectDao getSubjectDao() throws SQLException {
        return new SubjectDao(connectionSource);
    }

    public StatisticsDao getStatisticsDao() {
        return new StatisticsDao(connectionSource);
    }

}
