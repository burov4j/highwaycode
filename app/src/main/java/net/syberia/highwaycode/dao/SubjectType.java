package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "subject_types")
public class SubjectType implements Serializable {

    public static final byte RULES = 1, SIGNS = 2, MARKING = 3;

    @DatabaseField(id = true)
    private byte id;

    @DatabaseField(canBeNull = false)
    private String name;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
