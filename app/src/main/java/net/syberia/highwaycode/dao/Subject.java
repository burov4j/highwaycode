package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "subjects")
public class Subject implements Serializable {

    static final String FIELD_NAME_SUBJECT_TYPE_ID = "subject_type_id";

    @DatabaseField(id = true)
    private byte id;

    @DatabaseField
    private Byte number;

    @DatabaseField(canBeNull = false)
    private String title;

    @DatabaseField(columnName = "content_path")
    private String contentPath;

    // Используется наименование с нижним подчёркиванием, т.к. ORMLite не даёт задать поле columnName
    @DatabaseField(foreign = true, foreignColumnName = "id", canBeNull = false)
    private SubjectType subject_type;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public Byte getNumber() {
        return number;
    }

    public void setNumber(Byte number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SubjectType getSubjectType() {
        return subject_type;
    }

    public void setSubjectType(SubjectType subjectType) {
        this.subject_type = subjectType;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

}
