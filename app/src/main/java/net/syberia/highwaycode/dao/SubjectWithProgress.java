package net.syberia.highwaycode.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Andrey Burov
 */
@DatabaseTable(tableName = "subjects_with_progress")
public class SubjectWithProgress extends Subject {

    @DatabaseField(columnName = "questions_count")
    private byte countOfQuestions;

    @DatabaseField(columnName = "passed_questions_count")
    private byte countOfPassedQuestions;

    public byte getCountOfPassedQuestions() {
        return countOfPassedQuestions;
    }

    public void setCountOfPassedQuestions(byte countOfPassedQuestions) {
        this.countOfPassedQuestions = countOfPassedQuestions;
    }

    public byte getCountOfQuestions() {
        return countOfQuestions;
    }

    public void setCountOfQuestions(byte countOfQuestions) {
        this.countOfQuestions = countOfQuestions;
    }

}
