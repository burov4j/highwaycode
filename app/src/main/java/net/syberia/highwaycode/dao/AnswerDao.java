package net.syberia.highwaycode.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Burov
 */
public class AnswerDao extends BaseDaoImpl<Answer, Integer> {

    AnswerDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Answer.class);
    }

    public List<Answer> queryForQuestion(int questionId) throws SQLException {
        return queryForEq(Answer.FIELD_NAME_QUESTION_ID, questionId);
    }

}
