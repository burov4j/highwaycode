package net.syberia.highwaycode;

import android.content.Context;
import android.graphics.drawable.Drawable;

import net.syberia.highwaycode.dao.Question;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Andrey Burov
 */
class HighwayCodeImagesUtils {

    private HighwayCodeImagesUtils() {
        super();
    }

    static Drawable getImage(Context context, Question question) throws IOException {
        String questionsFolder;
      //  ApplicationProperties applicationProperties = new ApplicationProperties(context);
   //     if (applicationProperties.isUseNewTickets()) {
            questionsFolder = "questions_v2";
   /*     } else {
            questionsFolder = "questions";
        } */
        String imagePath = "images/" + questionsFolder + "/t" +
                question.getTicket().getNumber() + "q" + question.getNumber() + ".jpg";
        return getImage(context, imagePath);
    }

    static Drawable getImage(Context context, String imagePath) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(imagePath);
            return Drawable.createFromStream(inputStream, null);
        } catch (FileNotFoundException e) {
            return null;
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

}
