package net.syberia.highwaycode;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.SubjectWithProgress;

import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Burov
 */
abstract class SubjectsWithProgressAdapter extends RecyclerView.Adapter<SubjectsWithProgressAdapter.ViewHolder> implements OnSubjectClickListener {

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView titleTextView;
        private final TextView progressTextView;
        private final AppCompatImageView passedImageView;
        private final AppCompatImageView notPassedImageView;
        private final ProgressBar progressBar;

        private Subject subject;

        ViewHolder(View rootView) {
            super(rootView);
            this.titleTextView = (TextView) rootView.findViewById(R.id.subjectWithProgressTitleTextView);
            this.progressTextView = (TextView) rootView.findViewById(R.id.subjectProgressTextView);
            this.passedImageView = (AppCompatImageView) rootView.findViewById(R.id.subjectPassedImageView);
            this.notPassedImageView = (AppCompatImageView) rootView.findViewById(R.id.subjectNotPassedImageView);
            this.progressBar = (ProgressBar) rootView.findViewById(R.id.subjectProgressBar);
            int progressBarColor = ContextCompat.getColor(rootView.getContext(), R.color.material_indigo_500);
            this.progressBar.getProgressDrawable().setColorFilter(progressBarColor, PorterDuff.Mode.SRC_IN);
            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            SubjectsWithProgressAdapter.this.OnSubjectClick(view, subject, true);
        }

        void setPassed(boolean passed) {
            if (passed) {
                passedImageView.setVisibility(View.VISIBLE);
                notPassedImageView.setVisibility(View.INVISIBLE);
            } else {
                passedImageView.setVisibility(View.INVISIBLE);
                notPassedImageView.setVisibility(View.VISIBLE);
            }
        }

    }

    private final List<SubjectWithProgress> subjects;

    private final ApplicationProperties applicationProperties;

    SubjectsWithProgressAdapter(Context context, List<SubjectWithProgress> subjects) {
        applicationProperties = new ApplicationProperties(context);
        this.subjects = subjects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View rootView = LayoutInflater.from(context)
                .inflate(R.layout.subject_with_progress_item_view, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SubjectWithProgress subject = subjects.get(position);
        holder.subject = subject;
        holder.titleTextView.setText(subject.getTitle());
        int questionsCount = subject.getCountOfQuestions(),
                passedQuestionsCount = subject.getCountOfPassedQuestions();
        holder.progressTextView.setText(String.format(Locale.getDefault(),
                "Прогресс: %d из %d", passedQuestionsCount, questionsCount));
        int passPercentage = passedQuestionsCount * 100 / questionsCount;
        holder.progressBar.setProgress(passPercentage);
        if (passPercentage >= applicationProperties.getPassPercentage()) {
            holder.setPassed(true);
        } else {
            holder.setPassed(false);
        }
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }

}
