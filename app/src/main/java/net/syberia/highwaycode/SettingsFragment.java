package net.syberia.highwaycode;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

/**
 * @author Andrey Burov
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

}
