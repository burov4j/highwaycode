package net.syberia.highwaycode;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * @author Andrey Burov
 */
class ProgressWebClient extends WebChromeClient {

    private final ProgressBar progressBar;

    ProgressWebClient(ProgressBar progressBar) {
        this.progressBar = progressBar;
        progressBar.setVisibility(View.VISIBLE);
    }

    public void onProgressChanged(WebView view, int progress)
    {
        progressBar.setProgress(progress);
        if (progress >= 100) {
            progressBar.setVisibility(View.GONE);
        }
    }

}
