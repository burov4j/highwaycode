package net.syberia.highwaycode;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.syberia.highwaycode.dao.Ticket;
import net.syberia.highwaycode.dao.TicketWithProgress;

import java.util.List;
import java.util.Locale;

/**
 * @author Andrey Burov
 */
abstract class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.ViewHolder> implements OnTicketClickListener {

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView titleTextView;
        private final TextView ticketProgressTextView;
        private final AppCompatImageView passedImageView;
        private final AppCompatImageView notPassedImageView;
        private final AppCompatImageView durationPassedImageView;
        private final AppCompatImageView durationNotPassedImageView;
        private final AppCompatImageView ticketLockedImageView;
        private final ProgressBar ticketProgressBar;

        private Ticket ticket;

        ViewHolder(View rootView) {
            super(rootView);
            this.titleTextView = (TextView) rootView.findViewById(R.id.ticketTitleTextView);
            this.ticketProgressTextView = (TextView) rootView.findViewById(R.id.ticketProgressTextView);
            this.passedImageView = (AppCompatImageView) rootView.findViewById(R.id.ticketPassedImageView);
            this.notPassedImageView = (AppCompatImageView) rootView.findViewById(R.id.ticketNotPassedImageView);
            this.durationPassedImageView = (AppCompatImageView) rootView.findViewById(R.id.ticketDurationPassedImageView);
            this.durationNotPassedImageView = (AppCompatImageView) rootView.findViewById(R.id.ticketDurationNotPassedImageView);
            this.ticketLockedImageView = (AppCompatImageView) rootView.findViewById(R.id.ticketLockedImageView);
            this.ticketProgressBar = (ProgressBar) rootView.findViewById(R.id.ticketProgressBar);
            rootView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            TicketsAdapter.this.OnTicketClick(view, ticket);
        }

        void setPassed(boolean passed) {
            if (passed) {
                passedImageView.setVisibility(View.VISIBLE);
                notPassedImageView.setVisibility(View.INVISIBLE);
            } else {
                passedImageView.setVisibility(View.INVISIBLE);
                notPassedImageView.setVisibility(View.VISIBLE);
            }
        }

        void setDurationPassed(Boolean durationPassed) {
            if (durationPassed == null) {
                durationPassedImageView.setVisibility(View.INVISIBLE);
                durationNotPassedImageView.setVisibility(View.INVISIBLE);
            } else {
                if (durationPassed) {
                    durationPassedImageView.setVisibility(View.VISIBLE);
                    durationNotPassedImageView.setVisibility(View.INVISIBLE);
                } else {
                    durationPassedImageView.setVisibility(View.INVISIBLE);
                    durationNotPassedImageView.setVisibility(View.VISIBLE);
                }
            }
        }

        void setLocked(boolean locked) {
            if (locked) {
                setDurationPassed(null);
                passedImageView.setVisibility(View.INVISIBLE);
                notPassedImageView.setVisibility(View.INVISIBLE);
                ticketLockedImageView.setVisibility(View.VISIBLE);
                ticketProgressBar.setVisibility(View.INVISIBLE);
            } else {
                ticketLockedImageView.setVisibility(View.INVISIBLE);
                ticketProgressBar.setVisibility(View.VISIBLE);
            }
        }

    }

    private final List<TicketWithProgress> tickets;

    private final ApplicationProperties applicationProperties;

    TicketsAdapter(Context context, List<TicketWithProgress> tickets) {
        applicationProperties = new ApplicationProperties(context);
        this.tickets = tickets;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View rootView = LayoutInflater.from(context)
                .inflate(R.layout.ticket_item_view, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position >= tickets.size()) {
            holder.ticket = null;
            holder.titleTextView.setText(String.format(Locale.getDefault(), "Билет №%d", position + 1));
            holder.ticketProgressTextView.setText("Доступен в полной версии");
            holder.setLocked(true);
            return;
        }
        holder.setLocked(false);
        TicketWithProgress ticket = tickets.get(position);
        holder.ticket = ticket;
        byte ticketNumber = ticket.getNumber();
        holder.titleTextView.setText(String.format(Locale.getDefault(), "Билет №%d", ticketNumber));
        int questionsCount = ticket.getCountOfQuestions(),
                passedQuestionsCount = ticket.getCountOfPassedQuestions();
        holder.ticketProgressTextView.setText(String.format(Locale.getDefault(),
                "Прогресс: %d из %d", passedQuestionsCount, questionsCount));
        int passPercentage = passedQuestionsCount * 100 / questionsCount;
        holder.ticketProgressBar.setProgress(passPercentage);
        if (passPercentage >= applicationProperties.getPassPercentage()) {
            holder.setPassed(true);
        } else {
            holder.setPassed(false);
        }
        holder.setDurationPassed(ticket.getDurationPassed());
    }

    @Override
    public int getItemCount() {
        return 40;
    }

}
