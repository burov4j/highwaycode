package net.syberia.highwaycode;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * @author Andrey Burov
 */
public class AppVersionTextView extends AppCompatTextView {

    public AppVersionTextView(Context context, AttributeSet attrs) throws PackageManager.NameNotFoundException {
        super(context, attrs);
        String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        if (MainActivity.isFullVersion()) {
            setText(String.format("Полная версия (v%s)", versionName));
        } else {
            setText(String.format("Демоверсия (v%s)", versionName));
        }
    }

}
