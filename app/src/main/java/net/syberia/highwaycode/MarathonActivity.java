package net.syberia.highwaycode;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.Question;
import net.syberia.highwaycode.dao.QuestionDao;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Andrey Burov
 */
public class MarathonActivity extends AppCompatActivity
        implements QuestionFragment.OnAnswerSelectedListener {

    private static final String ANSWERED_QUESTIONS = "answered_questions";
    private static final String QUESTION = "question";

    private ArrayList<Integer> answeredQuestionsIds = new ArrayList<>();

    private Question question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.singleQuestionToolBar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(getResources().getString(R.string.app_name) + " ― Марафон");
        if (savedInstanceState != null && savedInstanceState.containsKey(QUESTION)) {
            question = (Question) savedInstanceState.getSerializable(QUESTION);
        } else {
            HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
            try {
                question = highwayCodeDatabaseHelper.getQuestionDao().queryForRandomQuestion();
            } catch (SQLException e) {
                throw new RuntimeException("Unable to get random question for marathon", e);
            } finally {
                highwayCodeDatabaseHelper.close();
            }
        }
        showQuestion(question);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(ANSWERED_QUESTIONS, answeredQuestionsIds);
        outState.putSerializable(QUESTION, question);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        answeredQuestionsIds = savedInstanceState.getIntegerArrayList(ANSWERED_QUESTIONS);
        question = (Question) savedInstanceState.getSerializable(QUESTION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.questions_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_hint:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Справка");
                alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                WebView webView = new WebView(this);
                webView.loadDataWithBaseURL("file:///android_asset/", question.getComment(),
                        "text/html", "UTF-8", null);
                alert.setView(webView);
                alert.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAnswerSelected(Question question, boolean success) {
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(this);
        try {
            QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
            question.setSuccess(success);
            questionDao.update(question);
            MistakesCountTextView.refresh(this);
            if (success) {
                answeredQuestionsIds.add(question.getId());
                int answeredQuestionsCount = answeredQuestionsIds.size();
                ApplicationProperties applicationProperties = new ApplicationProperties(this);
                if (answeredQuestionsCount > applicationProperties.getMarathonRecord()) {
                    applicationProperties.setMarathonRecord(answeredQuestionsCount);
                    MarathonRecordTextView.refresh(this);
                }
                if (answeredQuestionsCount < questionDao.countOf()) {
                    this.question = questionDao.queryForRandomQuestion(answeredQuestionsIds);
                    showQuestion(this.question);
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Поздравляем!");
                    alertDialog.setMessage(R.string.marathon_success_message);
                    alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            MarathonActivity.this.finish();
                        }
                    });
                    alertDialog.show();
                }
            } else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Ошибка");
                alertDialog.setMessage("Марафон окончен. \nВаш результат: " + answeredQuestionsIds.size() +
                        "\nВсего вопросов: " + questionDao.countOf());
                alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        MarathonActivity.this.finish();
                    }
                });
                alertDialog.show();
            }
        } catch (SQLException e) {
            Log.e(MarathonActivity.class.getName(), "Unable to process answer selection", e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
    }

    private void showQuestion(Question question) {
        QuestionFragment questionFragment = new QuestionFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(QuestionFragment.QUESTION, question);
        arguments.putBoolean(QuestionFragment.RANDOM_ANSWERS, true);
        questionFragment.setArguments(arguments);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.singleQuestionLayout, questionFragment)
                .commit();
    }

}
