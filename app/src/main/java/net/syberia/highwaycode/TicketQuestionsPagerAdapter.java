package net.syberia.highwaycode;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import net.syberia.highwaycode.dao.Question;

import java.util.List;

/**
 * @author Andrey Burov
 */
class TicketQuestionsPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Question> questions;

    TicketQuestionsPagerAdapter(FragmentManager fragmentManager, List<Question> questions) {
        super(fragmentManager);
        this.questions = questions;
    }

    @Override
    public Fragment getItem(int position) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle arguments = new Bundle();
        arguments.putSerializable(QuestionFragment.QUESTION, questions.get(position));
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

}
