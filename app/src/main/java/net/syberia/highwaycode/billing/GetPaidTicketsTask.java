package net.syberia.highwaycode.billing;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
public abstract class GetPaidTicketsTask extends AsyncTask<Void, Void, Boolean> {

    private final Context context;
    private final String purchaseData;
    private final String dataSignature;

    protected GetPaidTicketsTask(Context context, String purchaseData, String dataSignature) {
        this.context = context;
        this.purchaseData = purchaseData;
        this.dataSignature = dataSignature;
    }

    /**
     * Закомментированный код отвечает за загрузку старых билетов.
     * Необходим при одновременной поддержке старых и новых билетов.
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        HighwayCodeDatabaseHelper //oldHighwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getOldHighwayCodeDatabaseHelper(context),
                newHighwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getNewHighwayCodeDatabaseHelper(context);
        try {
           /* if (oldHighwayCodeDatabaseHelper.getTicketDao().countOf() == 20) {
                String oldPaidTicketsSql = PaidTicketsDao.downloadPaidTicketsSql(purchaseData, dataSignature, PaidTicketsDao.TicketsVersion.OLD);
                if (oldPaidTicketsSql == null) {
                    return false;
                } else {
                    oldHighwayCodeDatabaseHelper.executeMultipleSql(oldPaidTicketsSql);
                }
            } */
            if (newHighwayCodeDatabaseHelper.getTicketDao().countOf() == 20) {
                String newPaidTicketsSql = PaidTicketsDao.downloadPaidTicketsSql(purchaseData, dataSignature, PaidTicketsDao.TicketsVersion.NEW);
                if (newPaidTicketsSql == null) {
                    return false;
                } else {
                    newHighwayCodeDatabaseHelper.executeMultipleSql(newPaidTicketsSql);
                }
            }
        } catch (SQLException e) {
            Log.e(GetPaidTicketsTask.class.getName(), "Unable to install paid tickets", e);
            return false;
        } finally {
          //  oldHighwayCodeDatabaseHelper.close();
            newHighwayCodeDatabaseHelper.close();
        }
        return true;
    }

    @Override
    public abstract void onPostExecute(Boolean success);

}
