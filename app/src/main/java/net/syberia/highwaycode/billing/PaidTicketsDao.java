package net.syberia.highwaycode.billing;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

/**
 * @author Andrey Burov
 */
class PaidTicketsDao {

    private PaidTicketsDao() {
        super();
    }

    static String downloadPaidTicketsSql(String purchaseData, String dataSignature, TicketsVersion ticketsVersion) {
        RequestBody requestBody = new FormEncodingBuilder()
                .add("purchase_data", purchaseData)
                .add("data_signature", dataSignature)
                .build();
        Request request = new Request.Builder()
                .url(ticketsVersion.url)
                .post(requestBody)
                .build();
        OkHttpClient client = new OkHttpClient();
        InputStream bodyStream = null;
        try {
            Response response = client.newCall(request).execute();
            if (response.code() == 400) {
                return null;
            } else {
                bodyStream = response.body().byteStream();
                return unzipPaidTickets(bodyStream);
            }
        } catch (IOException e) {
            return null;
        } finally {
            IOUtils.closeQuietly(bodyStream);
        }
    }

    private static String unzipPaidTickets(InputStream inputStream) throws IOException {
        ZipInputStream zipInputStream = null;
        ByteArrayOutputStream outputStream = null;
        try {
            zipInputStream = new ZipInputStream(inputStream);
            zipInputStream.getNextEntry();
            outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = zipInputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }
            return new String(outputStream.toByteArray(), "UTF-8");
        } finally {
            IOUtils.closeQuietly(zipInputStream, outputStream);
        }
    }

    enum TicketsVersion {

        OLD("http://highwaycode.syberia.net/validation/index.php"),
        NEW("http://highwaycode.syberia.net/validation/v2/index.php");

        private final String url;

        TicketsVersion(String url) {
            this.url = url;
        }

    }

}
