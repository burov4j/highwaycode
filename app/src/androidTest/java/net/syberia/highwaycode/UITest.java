package net.syberia.highwaycode;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.rule.ActivityTestRule;

import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author Andrey Burov
 */
abstract class UITest {

    protected HighwayCodeDatabaseHelper highwayCodeDatabaseHelper;

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(InstrumentationRegistry.getTargetContext());
        try {
            onView(withText("Да")).perform(click());
        } catch (NoMatchingViewException ex) {
            // no welcome dialog
        }
    }

    @After
    public void tearDown() {
        highwayCodeDatabaseHelper.close();
    }

}
