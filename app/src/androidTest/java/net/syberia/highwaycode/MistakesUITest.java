package net.syberia.highwaycode;

import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;

import net.syberia.highwaycode.dao.Answer;
import net.syberia.highwaycode.dao.AnswerDao;
import net.syberia.highwaycode.dao.Question;
import net.syberia.highwaycode.dao.QuestionDao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * @author Andrey Burov
 */
public class MistakesUITest extends UITest {

    @Test
    public void fixMistakes() throws SQLException {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(R.id.tickets_menu_item));
        final byte ticketNumber = 5;
        onView(withId(R.id.progressRecyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition(ticketNumber - 1, click()));
        QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
        questionDao.clearProgress();
        AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
        List<Question> questions = questionDao.queryForTicket(ticketNumber),
                mistakenQuestions = new ArrayList<>();
        for (Question question : questions) {
            List<Answer> answers = answerDao.queryForQuestion(question.getId());
            Answer firstAnswer = answers.get(0);
            String firstAnswerButtonText = firstAnswer.getText();
            onView(withText(firstAnswerButtonText)).perform(click());
            if (question.getRightAnswerNumber() != firstAnswer.getNumber()) {
                mistakenQuestions.add(question);
            }
        }
        onView(withText("Завершить")).perform(click());
        onView(withId(R.id.mistakes_count_text_view))
                .check(matches(withText(String.valueOf(mistakenQuestions.size()))));
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(R.id.mistakes_menu_item));
        for (Question question : mistakenQuestions) {
            List<Answer> answers = answerDao.queryForQuestion(question.getId());
            for (Answer answer : answers) {
                if (question.getRightAnswerNumber() == answer.getNumber()) {
                    onView(withText(answer.getText())).perform(click());
                    break;
                }
            }
        }
        onView(withText("Завершить")).perform(click());
        onView(withId(R.id.mistakes_count_text_view)).check(matches(not(isDisplayed())));
    }

}
