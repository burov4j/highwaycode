package net.syberia.highwaycode;

import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.RecyclerViewActions;

import net.syberia.highwaycode.dao.Answer;
import net.syberia.highwaycode.dao.AnswerDao;
import net.syberia.highwaycode.dao.Question;
import net.syberia.highwaycode.dao.QuestionDao;
import net.syberia.highwaycode.dao.Subject;
import net.syberia.highwaycode.dao.SubjectDao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.containsString;

/**
 * @author Andrey Burov
 */
public class SubjectsWithProgressUITest extends UITest {

    @Test
    public void failSubject() throws SQLException {
        failSubject(1);
    }

    private void failSubject(int subjectIndex) throws SQLException {
        clickSubject(subjectIndex);
        QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
        AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
        SubjectDao subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
        Subject subject = subjectDao.queryForAllWithProgress().get(subjectIndex);
        questionDao.clearProgress();
        List<Question> questions = questionDao.queryForSubject(subject.getId());
        byte successAnswerCount = 0, failAnswerCount = 0;
        for (Question question : questions) {
            onView(withId(R.id.action_hint)).perform(click());
            onView(withText(R.string.ok)).perform(click());
            List<Answer> answers = answerDao.queryForQuestion(question.getId());
            Answer firstAnswer = answers.get(0);
            String firstAnswerButtonText = firstAnswer.getText();
            onView(withText(firstAnswerButtonText)).perform(click());
            if (question.getRightAnswerNumber() == firstAnswer.getNumber()) {
                successAnswerCount++;
            } else {
                failAnswerCount++;
            }
        }
        onView(withText(containsString(String.format("%s из %s", successAnswerCount, questions.size()))))
                .check(matches(isDisplayed()));
        onView(withText("Завершить")).perform(click());
        onView(withId(R.id.mistakes_count_text_view))
                .check(matches(withText(String.valueOf(failAnswerCount))));
    }

    @Test
    public void passSubject() throws SQLException {
        passSubject(2);
    }

    private void passSubject(int subjectIndex) throws SQLException {
        clickSubject(subjectIndex);
        QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
        AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
        SubjectDao subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
        Subject subject = subjectDao.queryForAllWithProgress().get(subjectIndex);
        List<Question> questions = questionDao.queryForSubject(subject.getId());
        for (Question question : questions) {
            List<Answer> answers = answerDao.queryForQuestion(question.getId());
            for (Answer answer : answers) {
                if (question.getRightAnswerNumber() == answer.getNumber()) {
                    onView(withText(answer.getText())).perform(click());
                    break;
                }
            }
        }
        int questionsSize = questions.size();
        onView(withText(containsString(String.format("%s из %s", questionsSize, questionsSize))))
                .check(matches(isDisplayed()));
        onView(withText("Завершить")).perform(click());
    }

    private void clickSubject(int subjectIndex) {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(R.id.subjects_menu_item));
        onView(withId(R.id.progressRecyclerView)).perform(RecyclerViewActions.actionOnItemAtPosition(subjectIndex, click()));
    }

}
