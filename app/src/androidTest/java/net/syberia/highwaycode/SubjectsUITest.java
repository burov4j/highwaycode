package net.syberia.highwaycode;

import android.support.test.espresso.contrib.DrawerActions;

import net.syberia.highwaycode.dao.SubjectDao;
import net.syberia.highwaycode.dao.SubjectType;

import org.junit.Test;

import java.sql.SQLException;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

/**
 * @author Andrey Burov
 */
public class SubjectsUITest extends UITest {

    @Test
    public void viewAllRules() throws SQLException {
        viewAllSubjects(SubjectType.RULES, R.id.highway_code_menu_item);
    }

    @Test
    public void viewAllSigns() throws SQLException {
        viewAllSubjects(SubjectType.SIGNS, R.id.signs_menu_item);
    }

    @Test
    public void viewAllMarking() throws SQLException {
        viewAllSubjects(SubjectType.MARKING, R.id.marking_menu_item);
    }

    private void viewAllSubjects(byte subjectTypeId, int menuResourceId) throws SQLException {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(menuResourceId));
        SubjectDao subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
        int subjectsCount = subjectDao.queryForSubjectTypeId(subjectTypeId).size();
        for (int position = 0; position < subjectsCount; position++) {
            onData(anything()).inAdapterView(withId(R.id.subjectsListView)).atPosition(position).perform(click());
            pressBack();
        }
    }

}
