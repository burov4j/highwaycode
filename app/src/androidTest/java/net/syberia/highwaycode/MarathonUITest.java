package net.syberia.highwaycode;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.DrawerActions;
import android.view.View;

import net.syberia.highwaycode.dao.Answer;
import net.syberia.highwaycode.dao.AnswerDao;
import net.syberia.highwaycode.dao.Question;
import net.syberia.highwaycode.dao.QuestionDao;

import org.hamcrest.Matcher;
import org.junit.Test;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class MarathonUITest extends UITest {

    @Test
    public void failMarathon() throws SQLException {
        clickStartMarathon();
        ApplicationProperties applicationProperties = new ApplicationProperties(InstrumentationRegistry.getTargetContext());
        applicationProperties.setMarathonRecord(0);
        AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
        QuestionDao questionDao = highwayCodeDatabaseHelper.getQuestionDao();
        questionDao.clearProgress();
        final int passedQuestionsCount = 5;
        for (int i = 0; i < passedQuestionsCount; i++) {
            onView(withId(R.id.action_hint)).perform(click());
            onView(withText(R.string.ok)).perform(click());
            answerQuestion(answerDao, true);
        }
        answerQuestion(answerDao, false);
        onView(withText(containsString("Ваш результат: " + passedQuestionsCount))).check(matches(isDisplayed()));
        onView(withText(R.string.ok)).perform(click());
        onView(withId(R.id.mistakes_count_text_view))
                .check(matches(withText(String.valueOf(1))));
        onView(withId(R.id.marathon_record_text_view))
                .check(matches(withText(String.valueOf(passedQuestionsCount))));
    }

    @Test
    public void passMarathon() throws SQLException {
        clickStartMarathon();
        Set<Integer> answeredQuestionIds = new HashSet<>();
        AnswerDao answerDao = highwayCodeDatabaseHelper.getAnswerDao();
        final long passedQuestionsCount = highwayCodeDatabaseHelper.getQuestionDao().countOf();
        for (int i = 0; i < passedQuestionsCount; i++) {
            int questionId = answerQuestion(answerDao, true);
            answeredQuestionIds.add(questionId);
        }
        if (answeredQuestionIds.size() != passedQuestionsCount) {
            fail("Some questions is duplicated in the marathon");
        }
        onView(withText(R.string.marathon_success_message)).check(matches(isDisplayed()));
        onView(withText(R.string.ok)).perform(click());
        onView(withId(R.id.mistakes_count_text_view)).check(matches(not(isDisplayed())));
        onView(withId(R.id.marathon_record_text_view))
                .check(matches(withText(String.valueOf(passedQuestionsCount))));
    }

    private void clickStartMarathon() {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(R.id.marathon_menu_item));
    }

    private int answerQuestion(AnswerDao answerDao, boolean right) throws SQLException {
        Question question = getQuestion(withId(R.id.questionNestedScrollView));
        List<Answer> answers = answerDao.queryForQuestion(question.getId());
        for (Answer answer : answers) {
            if (right && answer.getNumber() == question.getRightAnswerNumber()
                    || !right && answer.getNumber() != question.getRightAnswerNumber()) {
                onView(withText(answer.getText())).perform(click());
                break;
            }
        }
        return question.getId();
    }

    private Question getQuestion(final Matcher<View> matcher) {
        final Question[] questionHolder = {null};
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(View.class);
            }

            @Override
            public String getDescription() {
                return "Getting question from a tag of view";
            }

            @Override
            public void perform(UiController uiController, View view) {
                questionHolder[0] = (Question) view.getTag();
            }
        });
        return questionHolder[0];
    }

}
