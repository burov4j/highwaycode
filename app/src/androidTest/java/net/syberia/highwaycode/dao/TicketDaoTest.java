package net.syberia.highwaycode.dao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class TicketDaoTest extends DaoTest {

    private TicketDao ticketDao;

    @Override
    public void setUp() throws SQLException {
        super.setUp();
        ticketDao = highwayCodeDatabaseHelper.getTicketDao();
    }

    @Test
    public void queryForAllWithProgress() throws SQLException {
        List<TicketWithProgress> ticketsWithProgress = ticketDao.queryForAllWithProgress();
        TicketWithProgress ticketWithProgress = ticketsWithProgress.get(0);
        QuestionDao questionDao = new QuestionDao(ticketDao.getConnectionSource());
        questionDao.clearProgress();
        List<Question> questions = questionDao.queryForTicket(ticketWithProgress.getNumber());
        if (questions.size() != ticketWithProgress.getCountOfQuestions()) {
            fail("Wrong count of questions");
        }
        if (ticketWithProgress.getCountOfPassedQuestions() != 0) {
            fail("Wrong count of passed questions");
        }
        Question question = questions.get(0);
        question.setSuccess(true);
        questionDao.update(question);
        ticketsWithProgress = ticketDao.queryForAllWithProgress();
        ticketWithProgress = ticketsWithProgress.get(0);
        if (questions.size() != ticketWithProgress.getCountOfQuestions()) {
            fail("Wrong count of questions");
        }
        if (ticketWithProgress.getCountOfPassedQuestions() != 1) {
            fail("Wrong count of passed questions");
        }
    }

    @Test
    public void updateDurationPassed() throws SQLException {
        ticketDao.clearProgress();
        Ticket ticket = ticketDao.queryForAll().get(0);
        if (ticket.getDurationPassed() != null) {
            fail("The duration passed property of the ticket must be null in a new database");
        }
        ticketDao.updateDurationPassed(ticket.getNumber(), false);
        ticket = ticketDao.queryForAll().get(0);
        if (ticket.getDurationPassed()) {
            fail("Wrong duration passed of the ticket");
        }
    }

}
