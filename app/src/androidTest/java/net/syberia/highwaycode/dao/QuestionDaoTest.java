package net.syberia.highwaycode.dao;

import com.j256.ormlite.stmt.Where;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class QuestionDaoTest extends DaoTest {

    private QuestionDao questionDao;

    @Override
    public void setUp() throws SQLException {
        super.setUp();
        questionDao = highwayCodeDatabaseHelper.getQuestionDao();
    }

    @Test
    public void queryForRandomQuestion() throws SQLException {
        questionDao.queryForRandomQuestion();
    }

    @Test(timeout = 100)
    public void queryForRandomQuestionByMaximumExcludes() throws SQLException {
        List<Integer> maximumExcludes = new ArrayList<>();
        long countOfQuestions = questionDao.countOf();
        for (int i = 1; i < countOfQuestions; i++) {
            maximumExcludes.add(i);
        }
        questionDao.queryForRandomQuestion(maximumExcludes);
    }

    @Test
    public void queryForMistakesCount() throws SQLException {
        questionDao.clearProgress();
        long mistakesCount = questionDao.queryForMistakesCount();
        if (mistakesCount != 0) {
            fail("Mistakes count of a new database must be 0");
        }
        Question randomQuestion = questionDao.queryForRandomQuestion();
        randomQuestion.setSuccess(false);
        questionDao.update(randomQuestion);
        mistakesCount = questionDao.queryForMistakesCount();
        if (mistakesCount != 1) {
            fail("One question has mistake, but mistakes count = " + mistakesCount);
        }
    }

    @Test
    public void queryForMistaken() throws SQLException {
        questionDao.clearProgress();
        List<Question> mistakenQuestions = questionDao.queryForMistaken();
        if (!mistakenQuestions.isEmpty()) {
            fail("There are no mistaken questions in a new database");
        }
        Question randomQuestion = questionDao.queryForRandomQuestion();
        randomQuestion.setSuccess(false);
        questionDao.update(randomQuestion);
        mistakenQuestions = questionDao.queryForMistaken();
        int mistakenQuestionsCount = mistakenQuestions.size();
        if (mistakenQuestionsCount != 1) {
            fail("One question has mistake, but mistakes count = " + mistakenQuestionsCount);
        }
        if (mistakenQuestions.get(0).getId() != randomQuestion.getId()) {
            fail("Mistaken question id is not equal random question");
        }
    }

    @Test
    public void queryForTicket() throws SQLException {
        byte TICKET_NUMBER = 4;
        List<Question> questions = questionDao.queryForTicket(TICKET_NUMBER);
        for (Question question : questions) {
            if (question.getTicket().getNumber() != TICKET_NUMBER) {
                fail("Output ticket number is not equal the input");
            }
        }
    }

    @Test
    public void queryForSubject() throws SQLException {
        byte SUBJECT_ID = 10;
        List<Question> questions = questionDao.queryForSubject(SUBJECT_ID);
        QuestionsSubjectsLinksDao questionsSubjectsLinksDao = new QuestionsSubjectsLinksDao(questionDao.getConnectionSource());
        for (Question question : questions) {
            Where<QuestionsSubjectsLinks, Integer> linksQueryBuilder = questionsSubjectsLinksDao.queryBuilder()
                    .where().eq(QuestionsSubjectsLinks.FIELD_NAME_SUBJECT_ID, SUBJECT_ID).and()
                    .eq(QuestionsSubjectsLinks.FIELD_NAME_QUESTION_ID, question.getId());
            if (linksQueryBuilder.countOf() != 1) {
                fail("The question is not in the subject with id = " + SUBJECT_ID);
            }
        }
    }

}
