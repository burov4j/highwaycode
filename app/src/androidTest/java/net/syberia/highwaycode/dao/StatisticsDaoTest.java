package net.syberia.highwaycode.dao;

import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class StatisticsDaoTest extends DaoTest {

    private StatisticsDao statisticsDao;

    @Override
    public void setUp() throws SQLException {
        super.setUp();
        statisticsDao = highwayCodeDatabaseHelper.getStatisticsDao();
    }

    @Test
    public void getCustomStatistics() throws SQLException {
        QuestionDao questionDao = new QuestionDao(statisticsDao.getConnectionSource());
        questionDao.clearProgress();
        long questionsCount = questionDao.countOf();
        CustomStatistics customStatistics = statisticsDao.getCustomStatistics();
        if (customStatistics.getFailed() != 0 || customStatistics.getPassed() != 0
                || customStatistics.getNoResult() != questionsCount) {
            fail("Wrong custom statistics");
        }
        Question randomQuestion = questionDao.queryForRandomQuestion();
        randomQuestion.setSuccess(true);
        questionDao.update(randomQuestion);
        customStatistics = statisticsDao.getCustomStatistics();
        if (customStatistics.getFailed() != 0 || customStatistics.getPassed() != 1
                || customStatistics.getNoResult() != questionsCount - 1) {
            fail("Wrong custom statistics");
        }
        randomQuestion.setSuccess(false);
        questionDao.update(randomQuestion);
        customStatistics = statisticsDao.getCustomStatistics();
        if (customStatistics.getFailed() != 1 || customStatistics.getPassed() != 0
                || customStatistics.getNoResult() != questionsCount - 1) {
            fail("Wrong custom statistics");
        }
    }

}
