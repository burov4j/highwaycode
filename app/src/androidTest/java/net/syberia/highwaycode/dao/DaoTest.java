package net.syberia.highwaycode.dao;

import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
abstract class DaoTest {

    protected HighwayCodeDatabaseHelper highwayCodeDatabaseHelper;

    @Before
    public void setUp() throws SQLException {
        highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory.getHighwayCodeDatabaseHelper(InstrumentationRegistry.getTargetContext());
        highwayCodeDatabaseHelper.initDatabase();
    }

    @After
    public void tearDown() {
        highwayCodeDatabaseHelper.close();
    }

}
