package net.syberia.highwaycode.dao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class AnswerDaoTest extends DaoTest {

    private AnswerDao answerDao;

    @Override
    public void setUp() throws SQLException {
        super.setUp();
        answerDao = highwayCodeDatabaseHelper.getAnswerDao();
    }

    @Test
    public void queryForQuestion() throws SQLException {
        QuestionDao questionDao = new QuestionDao(answerDao.getConnectionSource());
        Question randomQuestion = questionDao.queryForRandomQuestion();
        List<Answer> answers = answerDao.queryForQuestion(randomQuestion.getId());
        for (Answer answer : answers) {
            if (answer.getQuestion().getId() != randomQuestion.getId()) {
                fail("Answer question id is not equal random question id");
            }
        }
    }

}
