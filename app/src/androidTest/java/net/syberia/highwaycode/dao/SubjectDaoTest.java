package net.syberia.highwaycode.dao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * @author Andrey Burov
 */
public class SubjectDaoTest extends DaoTest {

    private SubjectDao subjectDao;

    @Override
    public void setUp() throws SQLException {
        super.setUp();
        subjectDao = highwayCodeDatabaseHelper.getSubjectDao();
    }

    @Test
    public void queryForAllWithProgress() throws SQLException {
        List<SubjectWithProgress> subjects = subjectDao.queryForAllWithProgress();
        SubjectWithProgress subject = subjects.get(0);
        QuestionDao questionDao = new QuestionDao(subjectDao.getConnectionSource());
        questionDao.clearProgress();
        List<Question> questions = questionDao.queryForSubject(subject.getId());
        if (questions.size() != subject.getCountOfQuestions()) {
            fail("Wrong count of questions");
        }
        if (subject.getCountOfPassedQuestions() != 0) {
            fail("Wrong count of passed questions");
        }
        Question question = questions.get(0);
        question.setSuccess(true);
        questionDao.update(question);
        subjects = subjectDao.queryForAllWithProgress();
        subject = subjects.get(0);
        if (questions.size() != subject.getCountOfQuestions()) {
            fail("Wrong count of questions");
        }
        if (subject.getCountOfPassedQuestions() != 1) {
            fail("Wrong count of passed questions");
        }
    }

    @Test
    public void queryForSubjectTypeId() throws SQLException {
        subjectDao.queryForSubjectTypeId(SubjectType.RULES);
    }

}
