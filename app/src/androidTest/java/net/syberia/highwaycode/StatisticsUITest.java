package net.syberia.highwaycode;

import android.support.test.espresso.contrib.DrawerActions;

import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * @author Andrey Burov
 */
public class StatisticsUITest extends UITest {

    @Test
    public void showStatistics() {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(navigateTo(R.id.statistics_menu_item));
        onView(withText(R.string.statistics_tickets_tab_title)).perform(click());
        onView(withText(R.string.statistics_custom_tab_title)).perform(click());
    }

}
