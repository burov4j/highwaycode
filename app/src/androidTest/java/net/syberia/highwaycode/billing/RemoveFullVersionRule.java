package net.syberia.highwaycode.billing;

import android.support.test.InstrumentationRegistry;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import net.syberia.highwaycode.MainActivity;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseFactory;
import net.syberia.highwaycode.dao.HighwayCodeDatabaseHelper;
import net.syberia.highwaycode.dao.TicketDao;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.sql.SQLException;

/**
 * @author Andrey Burov
 */
class RemoveFullVersionRule implements TestRule {

    @Override
    public Statement apply(final Statement base, Description description) {
        BillingProcessor billingProcessor = new BillingProcessor(InstrumentationRegistry.getTargetContext(),
                MainActivity.hateThieves(), new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(String productId, TransactionDetails details) {
                // no operation
            }

            @Override
            public void onPurchaseHistoryRestored() {
                // no operation
            }

            @Override
            public void onBillingError(int errorCode, Throwable error) {
                // no operation
            }

            @Override
            public void onBillingInitialized() {
                // no operation
            }
        });
        billingProcessor.consumePurchase(MainActivity.TICKETS_PRODUCT_ID);
        HighwayCodeDatabaseHelper highwayCodeDatabaseHelper = HighwayCodeDatabaseFactory
                .getHighwayCodeDatabaseHelper(InstrumentationRegistry.getTargetContext());
        try {
            TicketDao ticketDao = highwayCodeDatabaseHelper.getTicketDao();
            ticketDao.deletePaidTickets();
        } catch (SQLException e) {
            throw new RuntimeException("Unable to delete paid tickets", e);
        } finally {
            highwayCodeDatabaseHelper.close();
        }
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                base.evaluate();
            }
        };
    }

}
