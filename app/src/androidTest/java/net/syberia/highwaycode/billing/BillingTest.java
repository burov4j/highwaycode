package net.syberia.highwaycode.billing;

import android.support.test.rule.ActivityTestRule;

import net.syberia.highwaycode.MainActivity;

import org.junit.Rule;

/**
 * @author Andrey Burov
 */
public class BillingTest {

    @Rule
    public RemoveFullVersionRule removeFullVersionRule = new RemoveFullVersionRule();

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

  /**
   * Приложение установлено впервые. Билеты с 21-40 недоступны. Пользователь покупает полную версию,
   * и на его телефон загружаются платные билеты.
   */
  //  @Test
    public void userStory1() {

    }

}
